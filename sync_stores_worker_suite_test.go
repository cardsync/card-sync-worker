package worker_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestSyncStoresWorker(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "SyncStoresWorker Suite")
}
