package main

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	_ "github.com/lib/pq"

	kitlog "github.com/go-kit/kit/log"
)

func main() {
	var err error

	var logger kitlog.Logger
	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "cron")
	}

	var psql *sql.DB
	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	var sess *session.Session
	{
		sess, err = session.NewSession(&aws.Config{
			Region: aws.String("us-east-1"),
		})
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not create aws session", "err", err)
			os.Exit(1)
		}
	}

	var client *sns.SNS
	{
		client = sns.New(sess)
	}

	var rows *sql.Rows

	rows, err = psql.Query(`SELECT user_card_condition_id, user_id FROM user_card_conditions_delayed_sync JOIN user_card_conditions ON user_card_conditions.id = user_card_condition_id WHERE sync_at <= now()`)

	for err == nil && rows.Next() {
		var u, user string

		err = rows.Scan(&u, &user)

		_, err = client.Publish(&sns.PublishInput{
			Message:  aws.String(fmt.Sprintf(`{"user_card_condition_id":"%s"}`, u)),
			TopicArn: aws.String("arn:aws:sns:us-east-1:797443717865:synkt-inventory-changed"),
		})

		if err != nil {
			logger.Log("level", "error", "msg", "error publishing sns message", "err", err, "user_id", user, "user_card_condition_id", u)
		} else {
			_, err = psql.Exec(`DELETE FROM user_card_conditions_delayed_sync WHERE user_card_condition_id = $1`, u)
		}

		if err != nil {
			logger.Log("level", "error", "msg", "error processing user_card_condition", "err", err, "user_id", user, "user_card_condition_id", u)
		} else {
			logger.Log("level", "info", "msg", "processed user_card_condition", "user_id", user, "user_card_condition_id", u)
		}
	}

	if err == nil {
		err = rows.Err()
	}

	if err != nil {
		logger.Log("level", "error", "msg", "error processing delayed sync", "err", err)
	}
}
