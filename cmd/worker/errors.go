package main

import (
	"fmt"

	"github.com/pkg/errors"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func getErrStacktrace(err error) *string {
	if err, ok := err.(stackTracer); ok {
		st := fmt.Sprintf("%+v", err.StackTrace())
		return &st
	}

	return nil
}
