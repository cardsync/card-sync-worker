package main

import (
	"time"

	"github.com/go-kit/kit/log"

	"gitlab.com/cardsync/card-sync-worker/ebay"
)

type loggingMiddleware struct {
	logger log.Logger
	next   ebay.Service
}

func (mw loggingMiddleware) Sync(u string) (err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"service", "ebay",
			"method", "sync",
			"input", u,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	err = mw.next.Sync(u)

	return
}

func (mw loggingMiddleware) RefreshListing(u string) (err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"service", "ebay",
			"method", "refreshListing",
			"input", u,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	err = mw.next.RefreshListing(u)

	return
}
