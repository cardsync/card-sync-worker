package main

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"

	"gitlab.com/cardsync/card-sync-worker/db"
	"gitlab.com/cardsync/card-sync-worker/ebay"
	"gitlab.com/cardsync/card-sync-worker/pkg/shopify"
	shopifyapi "gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"
	shopifylog "gitlab.com/cardsync/card-sync-worker/pkg/shopify/log"
)

type Message struct {
	UserCardConditionID string `json:"user_card_condition_id"`
	UserID              string `json:"user_id"`
}

func main() {
	var err error

	var logger kitlog.Logger
	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "pubsub")
	}

	var psql *sql.DB
	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	var repo db.Service
	{
		repo = db.NewService(psql)
	}

	stripe.Key = os.Getenv("STRIPE_API_KEY")
	stripe.DefaultLeveledLogger = stripeLogger{logger}

	var ebayAPI ebay.API
	{
		ebayAPI = ebay.NewAPI(
			os.Getenv("EBAY_API_URL"),
			os.Getenv("EBAY_API_APPLICATION_ID"),
			os.Getenv("EBAY_API_APPLICATION_SECRET"),
			repo.ReadToken,
			repo.SaveToken,
			repo.SaveOffer,
			repo.SaveListingID,
			repo.SaveEbayInteraction,
		)
	}

	var es ebay.Service
	{
		es = ebay.NewService(
			logger,
			repo.HasEbay,
			repo.ReadUserInventory,
			repo.ReadOfferID,
			ebayAPI.PutInventoryItem,
			ebayAPI.OfferPublished,
			ebayAPI.CreateOffer,
			ebayAPI.PublishOffer,
			ebayAPI.UpdateOffer,
			repo.SaveEbayInteraction,
			repo.SaveDelayedSync,
			repo.SetRefreshedAt,
		)
		es = loggingMiddleware{logger, es}
	}

	var ss shopify.Service
	{
		queryRow := func(query string, args ...interface{}) func(...interface{}) error {
			return psql.QueryRow(query, args...).Scan
		}
		ss = shopify.NewService(
			logger,
			repo.HasShopify,
			repo.ReadUserInventory,
			repo.SaveDelayedSync,

			shopifylog.FindCollection(logger, shopifyapi.FindCollection(queryRow, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.CreateCollection(logger, shopifyapi.CreateCollection(queryRow, psql.Exec, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.FindProduct(logger, shopifyapi.FindProduct(queryRow, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.CreateProduct(logger, shopifyapi.CreateProduct(queryRow, psql.Exec, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.FindProductVariant(logger, shopifyapi.FindProductVariant(queryRow, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.CreateProductVariant(logger, shopifyapi.CreateProductVariant(queryRow, psql.Exec, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.UpdateProductVariant(logger, shopifyapi.UpdateProductVariant(queryRow, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.UpdateProductVariantQuantity(logger, shopifyapi.UpdateProductVariantQuantity(queryRow, "2020-04", shopifyapi.GetLocation, shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
			shopifylog.AddProductToCollection(logger, shopifyapi.AddProductToCollection(queryRow, "2020-04", shopifyapi.RecordedClientDo(repo.SaveShopifyInteraction))),
		)
	}

	syncInventoryHandler := httptransport.NewServer(
		makeSyncInventoryEndpoint(repo, es, ss, logger),
		decodeSyncInventoryRequest(logger),
		encodeResponse,
	)

	refreshListingHandler := httptransport.NewServer(
		makeRefreshListingEndpoint(es, logger),
		decodeSyncInventoryRequest(logger),
		encodeResponse,
	)

	http.Handle("/pubsub", syncInventoryHandler)
	http.Handle("/sync-inventory", syncInventoryHandler)
	http.Handle("/refresh-listing", refreshListingHandler)
	logger.Log("msg", "HTTP", "addr", ":8080")
	logger.Log("err", http.ListenAndServe(":8080", nil))
}

func decodeSyncInventoryRequest(logger kitlog.Logger) func(_ context.Context, r *http.Request) (interface{}, error) {
	return func(_ context.Context, r *http.Request) (interface{}, error) {
		var (
			request syncInventoryRequest
			body    []byte
			err     error
		)

		body, err = ioutil.ReadAll(r.Body)
		if err != nil {
			return nil, err
		}

		logger.Log("level", "debug", "msg", "received request", "body", fmt.Sprintf("%s", body))

		r.Body = ioutil.NopCloser(bytes.NewBuffer(body))

		if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
			return nil, err
		}

		return request, nil
	}
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func makeSyncInventoryEndpoint(us db.Service, es ebay.Service, ss shopify.Service, logger kitlog.Logger) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		var (
			req syncInventoryRequest
			err error
		)

		req = request.(syncInventoryRequest)

		u, err := req.UserCardCondition()
		if err != nil {
			logger.Log("level", "error", "msg", "failed to get user card condition", "err", err)
			return pubsubResponse{err.Error()}, nil
		}

		if ok, err := us.UserInPlanLimit(u, os.Getenv("STRIPE_FREE_SUBSCRIPTION"), customer.Get); !ok && err == nil {
			_ = us.SaveEbayInteraction(u, "", []byte("Account over plan limit of inventory items"), nil)

			return pubsubResponse{"Account over plan limit of inventory items"}, nil
		} else if err != nil {
			logger.Log("level", "error", "msg", "failed to sync inventory", "err", err, "user_card_condition", u)
			return pubsubResponse{err.Error()}, nil
		}

		err = ss.Sync(u)
		if err != nil {
			logger.Log("level", "error", "msg", "failed to sync inventory", "err", err, "user_card_condition", u)
			return pubsubResponse{err.Error()}, nil
		}

		err = es.Sync(u)
		if err != nil {
			logger.Log("level", "error", "msg", "failed to sync inventory", "err", err, "user_card_condition", u)
			return pubsubResponse{err.Error()}, nil
		}

		return pubsubResponse{""}, nil
	}
}

func makeRefreshListingEndpoint(svc ebay.Service, logger kitlog.Logger) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		var (
			req     syncInventoryRequest
			offerID []byte

			err error
		)

		req = request.(syncInventoryRequest)

		offerID, err = base64.StdEncoding.DecodeString(req.Message.Data)
		if err != nil {
			return "", err
		}

		err = svc.RefreshListing(string(offerID))
		if err != nil {
			logger.Log("level", "error", "msg", "failed to refresh listing", "err", err, "offer_id", offerID)
			return pubsubResponse{err.Error()}, nil
		}

		return pubsubResponse{""}, nil
	}
}

type syncInventoryRequest struct {
	Message struct {
		Data string `json:"data"`
	} `json:"message"`
}

func (r syncInventoryRequest) UserCardCondition() (string, error) {
	var (
		m struct {
			UserCardCondition string `json:"user_card_condition_id"`
		}

		decoded []byte
		err     error
	)

	decoded, err = base64.StdEncoding.DecodeString(r.Message.Data)
	if err != nil {
		return "", err
	}

	err = json.NewDecoder(bytes.NewReader(decoded)).Decode(&m)
	if err != nil {
		return "", err
	}

	return m.UserCardCondition, err
}

type pubsubResponse struct {
	Err string `json:"err,omitempty"`
}
