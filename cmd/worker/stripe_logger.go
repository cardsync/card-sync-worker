package main

import (
	"fmt"

	kitlog "github.com/go-kit/kit/log"
)

type stripeLogger struct {
	logger kitlog.Logger
}

func (sl stripeLogger) Debugf(format string, v ...interface{}) {
	sl.logger.Log("level", "debug", "msg", fmt.Sprintf(format, v...))
}

func (sl stripeLogger) Errorf(format string, v ...interface{}) {
	sl.logger.Log("level", "error", "msg", fmt.Sprintf(format, v...))
}

func (sl stripeLogger) Infof(format string, v ...interface{}) {
	sl.logger.Log("level", "info", "msg", fmt.Sprintf(format, v...))
}

func (sl stripeLogger) Warnf(format string, v ...interface{}) {
	sl.logger.Log("level", "warn", "msg", fmt.Sprintf(format, v...))
}
