package main

import (
	"database/sql"
	"os"

	kitlog "github.com/go-kit/kit/log"
)

func main() {
	var err error

	var logger kitlog.Logger
	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "cron")
	}

	var psql *sql.DB
	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	var rows *sql.Rows

	rows, err = psql.Query(
		`SELECT user_card_condition_id, price
			FROM user_card_conditions
			JOIN ebay_offers ON user_card_conditions.id = user_card_condition_id
			WHERE refreshed_at <= now() - interval '25 days'`,
	)

	for err == nil && rows.Next() {
		var u, user string

		err = rows.Scan(&u, &user)
	}
}
