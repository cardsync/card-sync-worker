package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

func main() {
	var err error

	var psql *sql.DB
	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			panic(err)
		}
	}

	// Get an access token
	resp, err := http.PostForm("https://api.tcgplayer.com/token", url.Values{
		"grant_type":    {"client_credentials"},
		"client_id":     {os.Getenv("TCGPLAYER_PUBLIC_KEY")},
		"client_secret": {os.Getenv("TCGPLAYER_PRIVATE_KEY")},
	})
	if err != nil {
		panic(err)
	}

	if resp.StatusCode != 200 {
		panic(resp.StatusCode)
	}

	var auth requestTokenResponse

	err = json.NewDecoder(resp.Body).Decode(&auth)
	if err != nil {
		panic(err)
	}

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	// get all tcgplayer sets: https://api.tcgplayer.com/v1.27.0/catalog/categories/1/groups\?limit\=100 - iterate pages until results is empty
	var more bool = true
	var offset int = 0

	var groups []tcgplayerGroup = make([]tcgplayerGroup, 0)

	for {
		if !more {
			break
		}

		req, err := http.NewRequest("GET", fmt.Sprintf("https://api.tcgplayer.com/v1.27.0/catalog/categories/1/groups?limit=100&offset=%d", offset), nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", auth.AccessToken))

		resp, err = client.Do(req)
		if err != nil {
			panic(err)
		}

		var categoryGroups getCategoryGroupsResponse

		err = json.NewDecoder(resp.Body).Decode(&categoryGroups)
		if err != nil {
			panic(err)
		}

		for _, g := range categoryGroups.Results {
			groups = append(groups, g)
		}

		offset = offset + 100

		if len(categoryGroups.Results) == 0 {
			more = false
		}

		resp.Body.Close()
	}

	// iterate the tcg cards:  https://api.tcgplayer.com/v1.27.0/catalog/products/27071/skus, look for:
	//  - language: 1
	//  - conditions: 1, 2, 4
	//  - printing id: 1 (normal), 2 (foil)
	//
	// Try and find each card in our database and insert a relationship with the skuId
	for _, g := range groups {
		// See if we have a card set with this group name. if we don't, just skip
		var cardSetID string
		err = psql.QueryRow(`SELECT id FROM card_sets WHERE code = $1`, strings.ToLower(g.Abbreviation)).Scan(&cardSetID)
		if err == sql.ErrNoRows {
			continue
		}

		var (
			productsOffset int = 0
		)

		// Find each card in the tcgplayer api
		for {
			req, err := http.NewRequest("GET", fmt.Sprintf("https://api.tcgplayer.com/v1.27.0/catalog/products?categoryId=1&groupId=%d&limit=100&offset=%d", g.GroupID, productsOffset), nil)
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", auth.AccessToken))

			resp, err = client.Do(req)
			if err != nil {
				panic(err)
			}

			var products getProductsResponse

			err = json.NewDecoder(resp.Body).Decode(&products)
			if err != nil {
				panic(err)
			}

			if len(products.Results) == 0 {
				break
			}

			productsOffset = productsOffset + 100

			// Find each printing of the card from the criteria above and relate it to our card condition record
			for _, p := range products.Results {
				skuReq, err := http.NewRequest("GET", fmt.Sprintf("https://api.tcgplayer.com/v1.27.0/catalog/products/%d/skus", p.ProductID), nil)
				skuReq.Header.Set("Authorization", fmt.Sprintf("Bearer %s", auth.AccessToken))

				skuResp, err := client.Do(skuReq)
				if err != nil {
					panic(err)
				}

				var skus getSkusResponse

				err = json.NewDecoder(skuResp.Body).Decode(&skus)
				if err != nil {
					panic(err)
				}

				// Iterate each sku, if it matches the criteria, relate it to your card condition record
				for _, sku := range skus.Results {
					if sku.LanguageID == 1 {
						var conditionName string

						if sku.PrintingID == 1 && sku.ConditionID == 1 {
							conditionName = "near-mint"
						} else if sku.PrintingID == 1 && sku.ConditionID == 2 {
							conditionName = "slightly-played"
						} else if sku.PrintingID == 1 && sku.ConditionID == 4 {
							conditionName = "heavily-played"
						} else if sku.PrintingID == 2 && sku.ConditionID == 1 {
							conditionName = "foil-near-mint"
						} else if sku.PrintingID == 2 && sku.ConditionID == 2 {
							conditionName = "foil-slightly-played"
						} else if sku.PrintingID == 2 && sku.ConditionID == 4 {
							conditionName = "foil-heavily-played"
						} else {
							continue
						}

						var cardConditionID string

						// Look for a card condition
						err = psql.QueryRow(
							`SELECT card_conditions.id
							FROM card_conditions
							JOIN cards ON cards.id = card_id
							JOIN card_sets ON card_sets.id = card_set_id
							WHERE cards.name = $1
								AND card_sets.name = $2
								AND card_conditions.condition = $3`,
							p.Name, g.Name, conditionName,
						).Scan(&cardConditionID)
						if err == sql.ErrNoRows {
							fmt.Printf("Could not find %s %s %s\n", p.Name, g.Name, conditionName)
							continue
						} else if err != nil {
							panic(err)
						}

						_, err = psql.Exec(`INSERT INTO tcgplayer_card_conditions (card_condition_id, sku_id, created_at, updated_at) VALUES ($1, $2, now(), now()) ON CONFLICT DO NOTHING`, cardConditionID, sku.SkuID)
						if err != nil {
							panic(err)
						}
					}
				}

				skuResp.Body.Close()
			}
		}

		resp.Body.Close()
	}
}

type requestTokenResponse struct {
	AccessToken string `json:"access_token"`
}

type getCategoryGroupsResponse struct {
	Results []tcgplayerGroup
}

type tcgplayerGroup struct {
	GroupID      int    `json:"groupId"`
	Name         string `json:"name"`
	Abbreviation string `json:"abbreviation"`
}

type getProductsResponse struct {
	Results []struct {
		ProductID int    `json:"productId"`
		Name      string `json:"name"`
	}
}

type getSkusResponse struct {
	Results []tcgplayerSku
}

type tcgplayerSku struct {
	SkuID             int `json:"skuId"`
	LanguageID        int `json:"languageId"`
	PrintingID        int `json:"printingId"`
	ConditionID       int `json:"conditionId"`
	ProductName       string
	GroupName         string
	GroupAbbreviation string
}

type cardSet struct {
	ID   string
	Name string
	Code string
}
