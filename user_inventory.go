package worker

import "fmt"

type UserInventory struct {
	ID                  string
	UserID              string
	Qty                 int
	Price               int
	Condition           string
	CardName            string
	SetName             string
	OfferID             string
	CardID              string
	CardSetID           string
	Template            *string
	PaymentPolicyID     string
	ReturnPolicyID      string
	FulfillmentPolicyID string
	ListingQuantity     int
	Image               string
}

func (ui UserInventory) SKU() string {
	if ui.ListingQuantity == 1 {
		return ui.ID
	}

	return fmt.Sprintf("%s-4", ui.ID)
}

func (ui UserInventory) Quantity() int {
	if ui.ListingQuantity == 1 {
		return ui.oneQuantity()
	} else if ui.ListingQuantity == 4 {
		return ui.fourQuantity()
	}

	return ui.oneQuantity()
}

func (ui UserInventory) oneQuantity() int {
	if ui.Qty < 0 {
		return 0
	}

	if ui.Qty > 8 {
		return 8
	}

	return ui.Qty
}

func (ui UserInventory) fourQuantity() int {
	if ui.Qty < 0 {
		return 0
	}

	if ui.Qty >= 8 {
		return 2
	}

	if ui.Qty >= 4 {
		return 1
	}

	return 0
}

func (ui UserInventory) PhotoURL() string {
	return fmt.Sprintf("https://storage.googleapis.com/synkt-card-images/%s", ui.Image)
}

func (ui UserInventory) ListingQuantityText() string {
	if ui.ListingQuantity == 4 {
		return "four copies"
	}

	return "one copy"
}

func (ui UserInventory) EbayTitle() string {
	var title string

	title = fmt.Sprintf("%dx MTG %s - %s (%s)", ui.ListingQuantity, ui.CardName, ui.SetName, ui.Condition)

	if len(title) >= 80 {
		title = fmt.Sprintf("%s...", title[:77])
	}

	return title
}

func (ui UserInventory) PriceString() string {
	return fmt.Sprintf("$%.2f", float64(ui.Price)/100)
}

func (ui UserInventory) EbayPrice() float64 {
	return float64(ui.Price) / 100.0 * float64(ui.ListingQuantity)
}

func (ui UserInventory) ListableOnEbay() bool {
	return (ui.Price * ui.ListingQuantity) >= 99
}
