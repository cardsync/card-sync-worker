package api_test

import (
	"bytes"
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"

	worker "gitlab.com/cardsync/card-sync-worker"
)

var _ = Describe("ProductVariant", func() {
	var (
		db *sql.DB
		tx *sql.Tx

		queryRow func(query string, args ...interface{}) func(dest ...interface{}) error
		exec     func(string, ...interface{}) (sql.Result, error)

		pv ProductVariant
		ui worker.UserInventory

		apiVersion string = "api-version"
		shop       string = "test"
		username   string = "api-key"
		password   string = "password"

		shopifyResponseCode int

		recordInteraction       func(worker.UserInventory, *http.Request) (*http.Response, error)
		recordInteractionCalled bool
		recordInteractionErr    error

		err error
	)

	AssertRecordInteraction := func() {
		It("Executes and records the interaction", func() {
			Expect(recordInteractionCalled).To(BeTrue())
		})

		Describe("When executing and recording the interaction returns an error", func() {
			BeforeEach(func() {
				recordInteractionErr = errors.New("bang")
			})

			It("Returns the error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(recordInteractionErr))
			})

			It("Executes and records the interaction", func() {
				Expect(recordInteractionCalled).To(BeTrue())
			})
		})
	}

	DescribeWhenShopDoesNotExist := func() {
		Describe("When reading the shop returns an error", func() {
			var queryRowErr error

			BeforeEach(func() {
				queryRowErr = errors.New("bang")

				queryRow = func(query string, args ...interface{}) func(...interface{}) error {
					return func(...interface{}) error { return queryRowErr }
				}
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(queryRowErr))
			})
		})

		Describe("When shop does not exist", func() {
			BeforeEach(func() {
				queryRow = func(query string, args ...interface{}) func(...interface{}) error {
					return func(...interface{}) error { return sql.ErrNoRows }
				}
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(ErrNotFound))
			})
		})
	}

	DescribeShopifyReturnsNotFound := func() {
		Describe("When shopify returns not found", func() {
			BeforeEach(func() {
				shopifyResponseCode = http.StatusNotFound
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(ErrNotFound))
			})

			It("Executes and records the interaction", func() {
				Expect(recordInteractionCalled).To(BeTrue())
			})
		})
	}

	BeforeEach(func() {
		uri := os.Getenv("DATABASE_URL")
		Expect(uri).ToNot(Equal(""))

		db, err = sql.Open("postgres", uri)
		Expect(err).ToNot(HaveOccurred())

		tx, err = db.Begin()
		Expect(err).ToNot(HaveOccurred())

		ui, err = createUserInventory(tx)
		Expect(err).ToNot(HaveOccurred())

		shopifyResponseCode = http.StatusOK

		queryRow = func(query string, args ...interface{}) func(...interface{}) error {
			return tx.QueryRow(query, args...).Scan
		}
		exec = tx.Exec

		recordInteractionCalled = false
		recordInteractionErr = nil
	})

	AfterEach(func() {
		Expect(tx.Rollback()).ToNot(HaveOccurred())
	})

	Describe("FindProductVariant", func() {
		BeforeEach(func() {
			recordInteraction = func(pui worker.UserInventory, r *http.Request) (*http.Response, error) {
				Expect(pui).To(Equal(ui))
				recordInteractionCalled = true

				Expect(r.URL.String()).To(Equal(fmt.Sprintf("https://test.myshopify.com/admin/api/%s/variants/1.json", apiVersion)))
				Expect(r.Header.Get("Authorization")).To(Equal("Basic YXBpLWtleTpwYXNzd29yZA=="))
				Expect(r.Header.Get("Accept")).To(Equal("application/json"))
				Expect(r.Method).To(Equal("GET"))

				return &http.Response{
					StatusCode: shopifyResponseCode,
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"variant":{"id":1,"product_id":2,"title":"title","price":"price","inventory_item_id":3,"inventory_quantity":4}}`)),
				}, recordInteractionErr
			}
		})

		JustBeforeEach(func() {
			pv, err = FindProductVariant(
				queryRow, apiVersion, recordInteraction,
			)(ui)
		})

		Describe("When an error occurs reading the shopify user card condition", func() {
			var queryRowErr error

			BeforeEach(func() {
				queryRowErr = errors.New("bang")

				queryRow = func(query string, args ...interface{}) func(...interface{}) error {
					return func(...interface{}) error { return queryRowErr }
				}
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(queryRowErr))
			})
		})

		Describe("When there is no product variant id in the database", func() {
			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(ErrNotFound))
			})
		})

		Describe("When there is a product variant id in the database", func() {
			BeforeEach(func() {
				_, err = tx.Exec(`INSERT INTO shopify_user_card_conditions (
					user_card_condition_id, user_id, shopify_product_variant_id, created_at, updated_at
				) VALUES ($1, $2, 1, now(), now())`, ui.ID, ui.UserID)
				Expect(err).ToNot(HaveOccurred())
			})

			AssertRecordInteraction()

			It("Does not return an error", func() {
				Expect(err).ToNot(HaveOccurred())
			})

			It("Returns a shopify product variant", func() {
				Expect(pv).To(Equal(ProductVariant{
					ID:                1,
					ProductID:         2,
					Title:             "title",
					Price:             "price",
					InventoryItemID:   3,
					InventoryQuantity: 4,
				}))
			})

			DescribeShopifyReturnsNotFound()
		})
	})

	Describe("CreateProductVariant", func() {
		var (
			productID int = 2
		)

		BeforeEach(func() {
			recordInteraction = func(pui worker.UserInventory, r *http.Request) (*http.Response, error) {
				Expect(pui).To(Equal(ui))
				recordInteractionCalled = true

				Expect(r.URL.String()).To(Equal(fmt.Sprintf("https://test.myshopify.com/admin/api/%s/products/%d/variants.json", apiVersion, productID)))
				Expect(r.Header.Get("Authorization")).To(Equal("Basic YXBpLWtleTpwYXNzd29yZA=="))
				Expect(r.Header.Get("Accept")).To(Equal("application/json"))
				Expect(r.Method).To(Equal("POST"))

				return &http.Response{
					StatusCode: shopifyResponseCode,
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"variant":{"id":1,"product_id":2,"title":"title","price":"price","inventory_item_id":3,"inventory_quantity":4}}`)),
				}, recordInteractionErr
			}
		})

		JustBeforeEach(func() {
			pv, err = CreateProductVariant(
				queryRow, exec, apiVersion, recordInteraction,
			)(ui.UserID, productID, ui)
		})

		DescribeWhenShopDoesNotExist()

		Describe("When there is no shop in the database", func() {
			BeforeEach(func() {
				_, err = tx.Exec(`TRUNCATE user_shopifies`)
				Expect(err).ToNot(HaveOccurred())
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(ErrNotFound))
			})
		})

		It("Does not return an error", func() {
			Expect(err).ToNot(HaveOccurred())
		})

		AssertRecordInteraction()

		It("Creates a shopify_user_card_condition record", func() {
			var (
				userID, userCardConditionID string
				shopifyProductVariantID     int
			)

			err = tx.QueryRow(`SELECT user_id, user_card_condition_id, shopify_product_variant_id FROM shopify_user_card_conditions WHERE user_card_condition_id = $1`, ui.ID).Scan(&userID, &userCardConditionID, &shopifyProductVariantID)
			Expect(err).ToNot(HaveOccurred())
			Expect(userCardConditionID).To(Equal(ui.ID))
			Expect(userID).To(Equal(ui.UserID))
		})

		Describe("When executing and recording the interaction returns an error", func() {
			BeforeEach(func() {
				recordInteractionErr = errors.New("bang")
			})

			It("Does not create a shopify_user_card_conditions record", func() {
				var one int
				err = tx.QueryRow(`SELECT 1 FROM shopify_user_card_conditions WHERE user_card_condition_id = $1`, ui.ID).Scan(&one)
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(sql.ErrNoRows))
			})
		})
	})

	Describe("UpdateProductVariant", func() {
		var (
			shopifyResponseCode int = http.StatusNoContent
		)

		BeforeEach(func() {
			pv = ProductVariant{ID: 1}

			recordInteraction = func(pui worker.UserInventory, r *http.Request) (*http.Response, error) {
				Expect(pui).To(Equal(ui))
				recordInteractionCalled = true

				Expect(r.URL.String()).To(Equal(fmt.Sprintf("https://test.myshopify.com/admin/api/%s/variants/%d.json", apiVersion, pv.ID)))
				Expect(r.Header.Get("Authorization")).To(Equal("Basic YXBpLWtleTpwYXNzd29yZA=="))
				Expect(r.Header.Get("Accept")).To(Equal("application/json"))
				Expect(r.Method).To(Equal("PUT"))

				bodyBytes, _ := ioutil.ReadAll(r.Body)
				Expect(string(bodyBytes)).To(Equal(fmt.Sprintf(`{"variant":{"id":"%d","option1":"%s","price":"%s"}}`, pv.ID, ui.Condition, ui.PriceString())))

				return &http.Response{
					StatusCode: shopifyResponseCode,
					Body:       nil,
				}, recordInteractionErr
			}
		})

		JustBeforeEach(func() {
			err = UpdateProductVariant(
				queryRow, apiVersion, recordInteraction,
			)(ui, pv)
		})

		It("Does not return an error", func() {
			Expect(err).ToNot(HaveOccurred())
		})

		DescribeWhenShopDoesNotExist()
		AssertRecordInteraction()

		Describe("When Shopify returns an error", func() {
			BeforeEach(func() {
				shopifyResponseCode = http.StatusBadRequest
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
			})
		})
	})

	Describe("UpdateProductVariantQuantity", func() {
		var (
			shopifyResponseCode int = http.StatusNoContent
			getLocation         func(worker.UserInventory, string, string, string, string, func(worker.UserInventory, *http.Request) (*http.Response, error)) (int, error)
			locationID          int
			getLocationErr      error
		)

		BeforeEach(func() {
			pv = ProductVariant{ID: 1, InventoryItemID: 2}
			getLocationErr = nil

			locationID = 1234

			getLocation = func(
				pui worker.UserInventory,
				pShop, pUsername, pPassword, apiVersion string,
				pRecordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
			) (int, error) {
				Expect(pui).To(Equal(ui))
				Expect(pShop).To(Equal(shop))
				Expect(pUsername).To(Equal(username))
				Expect(pPassword).To(Equal(password))

				return locationID, getLocationErr
			}

			recordInteraction = func(pui worker.UserInventory, r *http.Request) (*http.Response, error) {
				Expect(pui).To(Equal(ui))
				recordInteractionCalled = true

				Expect(r.URL.String()).To(Equal(fmt.Sprintf("https://test.myshopify.com/admin/api/%s/inventory_levels/set.json", apiVersion)))
				Expect(r.Header.Get("Authorization")).To(Equal("Basic YXBpLWtleTpwYXNzd29yZA=="))
				Expect(r.Header.Get("Accept")).To(Equal("application/json"))
				Expect(r.Method).To(Equal("POST"))

				bodyBytes, _ := ioutil.ReadAll(r.Body)
				Expect(string(bodyBytes)).To(Equal(fmt.Sprintf(`{"location_id":%d,"inventory_item_id":%d,"available":%d}`, locationID, pv.InventoryItemID, ui.Quantity())))

				return &http.Response{
					StatusCode: shopifyResponseCode,
					Body:       nil,
				}, recordInteractionErr
			}
		})

		JustBeforeEach(func() {
			err = UpdateProductVariantQuantity(
				queryRow, apiVersion, getLocation, recordInteraction,
			)(ui, pv)
		})

		It("Does not return an error", func() {
			Expect(err).ToNot(HaveOccurred())
		})

		DescribeWhenShopDoesNotExist()
		AssertRecordInteraction()

		Describe("When Shopify returns an error", func() {
			BeforeEach(func() {
				shopifyResponseCode = http.StatusBadRequest
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
			})
		})
	})
})
