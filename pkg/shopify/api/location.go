package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	worker "gitlab.com/cardsync/card-sync-worker"
)

func GetLocation(
	ui worker.UserInventory,
	shop, username, password, apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) (int, error) {
	var (
		req  *http.Request
		resp *http.Response

		err error
	)

	req, err = http.NewRequest("GET", fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/locations.json", shop, apiVersion), nil)
	if err != nil {
		return 0, errors.Wrap(err, "error creating http client")
	}

	req.SetBasicAuth(username, password)
	req.Header.Add("Accept", "application/json")

	resp, err = recordInteraction(ui, req)
	if err != nil {
		return 0, errors.Wrap(err, "error executing http call")
	}

	if resp.StatusCode == http.StatusNotFound {
		return 0, ErrNotFound
	}

	var locations struct {
		Locations []struct {
			ID int
		} `json:"locations"`
	}

	err = json.NewDecoder(resp.Body).Decode(&locations)

	return locations.Locations[0].ID, errors.Wrap(err, "error decoding json")
}
