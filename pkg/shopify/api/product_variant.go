package api

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	worker "gitlab.com/cardsync/card-sync-worker"
)

type ProductVariant struct {
	ID                int    `json:"id"`
	ProductID         int    `json:"product_id"`
	Title             string `json:"title"`
	Price             string `json:"price"`
	InventoryItemID   int    `json:"inventory_item_id"`
	InventoryQuantity int    `json:"inventory_quantity"`
}

// FindProductVariant finds a shopify product variant object based on a synkt user card condition id
func FindProductVariant(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory) (ProductVariant, error) {
	return func(ui worker.UserInventory) (ProductVariant, error) {
		var (
			shop, username, password string
			productVariantID         int

			err error
		)

		err = queryRow(`SELECT
			shopify_product_variant_id,
			shop, api_key, password
			FROM shopify_user_card_conditions
			JOIN user_shopifies ON user_shopifies.user_id = shopify_user_card_conditions.user_id
			WHERE user_shopifies.user_id = $1 AND user_card_condition_id = $2`,
			ui.UserID, ui.ID,
		)(&productVariantID, &shop, &username, &password)

		if err == sql.ErrNoRows {
			return ProductVariant{}, ErrNotFound
		} else if err != nil {
			return ProductVariant{}, errors.Wrap(err, "error reading shopify product variant")
		}

		var (
			req  *http.Request
			resp *http.Response
		)

		req, err = http.NewRequest("GET", fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/variants/%d.json", shop, apiVersion, productVariantID), nil)
		if err != nil {
			return ProductVariant{}, errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return ProductVariant{}, errors.Wrap(err, "error executing http call")
		}

		if resp.StatusCode == http.StatusNotFound {
			return ProductVariant{}, ErrNotFound
		}

		var productVariant struct {
			Variant ProductVariant `json:"variant"`
		}

		err = json.NewDecoder(resp.Body).Decode(&productVariant)

		return productVariant.Variant, errors.Wrap(err, "error decoding json")
	}
}

// CreateProductVariant creates a shopify product variant object based on a synkt card id
func CreateProductVariant(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	exec func(string, ...interface{}) (sql.Result, error),
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(string, int, worker.UserInventory) (ProductVariant, error) {
	return func(userID string, productID int, ui worker.UserInventory) (ProductVariant, error) {
		var (
			shop, condition    string
			username, password string
			pv                 ProductVariant

			req  *http.Request
			resp *http.Response

			err error
		)

		err = queryRow(`SELECT
			shop, api_key, password
			FROM user_shopifies
			WHERE user_id = $1`,
			userID,
		)(&shop, &username, &password)

		if err == sql.ErrNoRows {
			return pv, ErrNotFound
		} else if err != nil {
			return pv, errors.Wrap(err, "error reading shop")
		}

		err = queryRow(`SELECT
			condition
			FROM card_conditions
			JOIN user_card_conditions ON card_conditions.id = user_card_conditions.card_condition_id
			WHERE user_card_conditions.id = $1`,
			ui.ID,
		)(&condition)

		if err == sql.ErrNoRows {
			return pv, ErrNotFound
		} else if err != nil {
			return pv, errors.Wrap(err, "error reading card name")
		}

		req, err = http.NewRequest(
			"POST",
			fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/products/%d/variants.json", shop, apiVersion, productID),
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"variant":{"option1":"%s","price":"%s","inventory_management": "shopify"}}`, condition, ui.PriceString()))),
		)
		if err != nil {
			return pv, errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return pv, errors.Wrap(err, "error executing http call")
		}

		var productVariant struct {
			Variant ProductVariant `json:"variant"`
		}

		err = json.NewDecoder(resp.Body).Decode(&productVariant)

		_, err = exec(`INSERT INTO shopify_user_card_conditions (
			user_card_condition_id, shopify_product_variant_id, user_id, created_at, updated_at
		) VALUES ($1, $2, $3, now(), now())`, ui.ID, productVariant.Variant.ID, ui.UserID)

		return productVariant.Variant, errors.Wrap(err, "error creating shopify user card condition")
	}
}

// UpdateProductVariant updates a shopify product variant object with a new price
func UpdateProductVariant(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory, ProductVariant) error {
	return func(ui worker.UserInventory, pv ProductVariant) error {
		var (
			shop, username, password string

			req  *http.Request
			resp *http.Response

			err error
		)

		err = queryRow(`SELECT
			shop, api_key, password
			FROM user_shopifies
			WHERE user_id = $1`,
			ui.UserID,
		)(&shop, &username, &password)

		if err == sql.ErrNoRows {
			return ErrNotFound
		} else if err != nil {
			return errors.Wrap(err, "error reading shop")
		}

		req, err = http.NewRequest(
			"PUT",
			fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/variants/%d.json", shop, apiVersion, pv.ID),
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"variant":{"id":"%d","option1":"%s","price":"%s"}}`, pv.ID, ui.Condition, ui.PriceString()))),
		)
		if err != nil {
			return errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return errors.Wrap(err, "error executing http call")
		}

		if resp.StatusCode > http.StatusNoContent {
			return errors.New("error executing http call")
		}

		return nil
	}
}

// UpdateProductVariantQuantity updates a shopify product variant object with a new quantity
func UpdateProductVariantQuantity(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	apiVersion string,
	getLocation func(worker.UserInventory, string, string, string, string, func(worker.UserInventory, *http.Request) (*http.Response, error)) (int, error),
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory, ProductVariant) error {
	return func(ui worker.UserInventory, pv ProductVariant) error {
		var (
			shop, username, password string
			locationID               int

			req  *http.Request
			resp *http.Response

			err error
		)

		err = queryRow(`SELECT
			shop, api_key, password
			FROM user_shopifies
			WHERE user_id = $1`,
			ui.UserID,
		)(&shop, &username, &password)

		if err == sql.ErrNoRows {
			return ErrNotFound
		} else if err != nil {
			return errors.Wrap(err, "error reading shop")
		}

		locationID, err = getLocation(ui, shop, username, password, apiVersion, recordInteraction)
		if err != nil {
			return errors.Wrap(err, "error finding location")
		}

		req, err = http.NewRequest(
			"POST",
			fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/inventory_levels/set.json", shop, apiVersion),
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"location_id":%d,"inventory_item_id":%d,"available":%d}`, locationID, pv.InventoryItemID, ui.Quantity()))),
		)
		if err != nil {
			return errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return errors.Wrap(err, "error executing http call")
		}

		if resp.StatusCode > http.StatusNoContent {
			return errors.New("error executing http call")
		}

		return nil
	}
}
