package api

import "errors"

var ErrNotFound = errors.New("Not Found")
