package api_test

import (
	"bytes"
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"

	worker "gitlab.com/cardsync/card-sync-worker"
)

var _ = Describe("Product", func() {
	var (
		db *sql.DB
		tx *sql.Tx

		queryRow func(query string, args ...interface{}) func(dest ...interface{}) error
		exec     func(string, ...interface{}) (sql.Result, error)

		p  Product
		ui worker.UserInventory

		apiVersion string = "api-version"
		//shop       string = "test"
		//username   string = "api-key"
		//password   string = "password"

		shopifyResponseCode int

		productID int = 11

		recordInteraction       func(worker.UserInventory, *http.Request) (*http.Response, error)
		recordInteractionCalled bool
		recordInteractionErr    error

		err error
	)

	AssertRecordInteraction := func() {
		It("Executes and records the interaction", func() {
			Expect(recordInteractionCalled).To(BeTrue())
		})

		Describe("When executing and recording the interaction returns an error", func() {
			BeforeEach(func() {
				recordInteractionErr = errors.New("bang")
			})

			It("Returns the error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(recordInteractionErr))
			})

			It("Executes and records the interaction", func() {
				Expect(recordInteractionCalled).To(BeTrue())
			})
		})
	}

	DescribeWhenShopDoesNotExist := func() {
		Describe("When reading the shop returns an error", func() {
			var queryRowErr error

			BeforeEach(func() {
				queryRowErr = errors.New("bang")

				queryRow = func(query string, args ...interface{}) func(...interface{}) error {
					return func(...interface{}) error { return queryRowErr }
				}
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(queryRowErr))
			})
		})

		Describe("When shop does not exist", func() {
			BeforeEach(func() {
				queryRow = func(query string, args ...interface{}) func(...interface{}) error {
					return func(...interface{}) error { return sql.ErrNoRows }
				}
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(ErrNotFound))
			})
		})
	}

	DescribeShopifyReturnsNotFound := func() {
		Describe("When shopify returns not found", func() {
			BeforeEach(func() {
				shopifyResponseCode = http.StatusNotFound
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(ErrNotFound))
			})

			It("Executes and records the interaction", func() {
				Expect(recordInteractionCalled).To(BeTrue())
			})
		})
	}

	BeforeEach(func() {
		uri := os.Getenv("DATABASE_URL")
		Expect(uri).ToNot(Equal(""))

		db, err = sql.Open("postgres", uri)
		Expect(err).ToNot(HaveOccurred())

		tx, err = db.Begin()
		Expect(err).ToNot(HaveOccurred())

		ui, err = createUserInventory(tx)
		Expect(err).ToNot(HaveOccurred())

		shopifyResponseCode = http.StatusOK

		queryRow = func(query string, args ...interface{}) func(...interface{}) error {
			return tx.QueryRow(query, args...).Scan
		}
		exec = tx.Exec

		recordInteractionCalled = false
		recordInteractionErr = nil
	})

	AfterEach(func() {
		Expect(tx.Rollback()).ToNot(HaveOccurred())
	})

	Describe("FindProduct", func() {
		BeforeEach(func() {
			recordInteraction = func(pui worker.UserInventory, r *http.Request) (*http.Response, error) {
				Expect(pui).To(Equal(ui))
				recordInteractionCalled = true

				Expect(r.URL.String()).To(Equal(fmt.Sprintf("https://test.myshopify.com/admin/api/%s/products/%d.json", apiVersion, productID)))
				Expect(r.Header.Get("Authorization")).To(Equal("Basic YXBpLWtleTpwYXNzd29yZA=="))
				Expect(r.Header.Get("Accept")).To(Equal("application/json"))
				Expect(r.Method).To(Equal("GET"))

				return &http.Response{
					StatusCode: shopifyResponseCode,
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"product":{"id":11,"title":"title"}}`)),
				}, recordInteractionErr
			}
		})

		JustBeforeEach(func() {
			p, err = FindProduct(
				queryRow, apiVersion, recordInteraction,
			)(ui)
		})

		Describe("When an error occurs reading the shopify user card", func() {
			var queryRowErr error

			BeforeEach(func() {
				queryRowErr = errors.New("bang")

				queryRow = func(query string, args ...interface{}) func(...interface{}) error {
					return func(...interface{}) error { return queryRowErr }
				}
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(errors.Cause(err)).To(Equal(queryRowErr))
			})
		})

		Describe("When there is no data in the database", func() {
			BeforeEach(func() {
				_, err = tx.Exec(`TRUNCATE shopify_user_cards`)
				Expect(err).ToNot(HaveOccurred())
			})

			It("Returns an error", func() {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(ErrNotFound))
			})
		})

		DescribeWhenShopDoesNotExist()
		DescribeShopifyReturnsNotFound()
		AssertRecordInteraction()

		It("Does not return an error", func() {
			Expect(err).ToNot(HaveOccurred())
		})

		It("Returns a product", func() {
			Expect(p).To(Equal(Product{ID: productID, Title: "title"}))
		})
	})

	Describe("CreateProduct", func() {
		BeforeEach(func() {
			_, err = tx.Exec(`TRUNCATE shopify_user_cards`)
			Expect(err).ToNot(HaveOccurred())

			recordInteraction = func(pui worker.UserInventory, r *http.Request) (*http.Response, error) {
				Expect(pui).To(Equal(ui))
				recordInteractionCalled = true

				Expect(r.URL.String()).To(Equal(fmt.Sprintf("https://test.myshopify.com/admin/api/%s/products.json", apiVersion)))
				Expect(r.Header.Get("Authorization")).To(Equal("Basic YXBpLWtleTpwYXNzd29yZA=="))
				Expect(r.Header.Get("Accept")).To(Equal("application/json"))
				Expect(r.Method).To(Equal("POST"))

				bodyBytes, _ := ioutil.ReadAll(r.Body)
				Expect(string(bodyBytes)).To(Equal(fmt.Sprintf(
					`{"product":{"title":"%s","variants":[{"option1":"%s","price":"%s","inventory_management": "shopify"}],"images":[{"src":"%s"}]}}`,
					ui.CardName, ui.Condition, ui.PriceString(), ui.PhotoURL(),
				)))

				return &http.Response{
					StatusCode: shopifyResponseCode,
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"product":{"id":11,"title":"title","variants":[{"id":11}]}}`)),
				}, recordInteractionErr
			}
		})

		JustBeforeEach(func() {
			p, err = CreateProduct(
				queryRow, exec, apiVersion, recordInteraction,
			)(ui)
		})

		DescribeWhenShopDoesNotExist()
		AssertRecordInteraction()

		It("Does not return an error", func() {
			Expect(err).ToNot(HaveOccurred())
		})

		It("Returns a product", func() {
			Expect(p).To(Equal(Product{ID: productID, Title: "title", Variants: []ProductVariant{{ID: 11}}}))
		})
	})
})
