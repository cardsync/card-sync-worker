package api

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"

	worker "gitlab.com/cardsync/card-sync-worker"
)

type Collection struct {
	ID     int    `json:"id"`
	Handle string `json:"handle"`
	Title  string `json:"title"`
}

// FindCollection finds a shopify collection object based on a synkt set id
func FindCollection(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory) (Collection, error) {
	return func(ui worker.UserInventory) (Collection, error) {
		var (
			shop, username, password string
			collectionID             int

			c Collection

			err error
		)

		err = queryRow(`SELECT
			shopify_collection_id,
			shop, api_key, password
			FROM shopify_user_card_sets
			JOIN user_shopifies ON user_shopifies.user_id = shopify_user_card_sets.user_id
			WHERE shopify_user_card_sets.user_id = $1 AND card_set_id = $2`,
			ui.UserID, ui.CardSetID,
		)(&collectionID, &shop, &username, &password)

		if err == sql.ErrNoRows {
			return c, ErrNotFound
		} else if err != nil {
			return c, errors.Wrap(err, "error reading shopify collection")
		}

		var (
			req  *http.Request
			resp *http.Response
		)

		req, err = http.NewRequest("GET", fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/custom_collections/%d.json", shop, apiVersion, collectionID), nil)
		if err != nil {
			return c, errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return c, errors.Wrap(err, "error executing http call")
		}

		if resp.StatusCode == http.StatusNotFound {
			return c, ErrNotFound
		}

		var collection struct {
			CustomCollection Collection `json:"custom_collection"`
		}

		err = json.NewDecoder(resp.Body).Decode(&collection)

		return collection.CustomCollection, errors.Wrap(err, "error decoding json")
	}
}

// CreateCollection finds a shopify collection object based on a synkt set id
func CreateCollection(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	exec func(string, ...interface{}) (sql.Result, error),
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory) (Collection, error) {
	return func(ui worker.UserInventory) (Collection, error) {
		var (
			shop, username, password string
			c                        Collection

			req  *http.Request
			resp *http.Response

			err error
		)

		err = queryRow(`SELECT
			shop, api_key, password
			FROM user_shopifies
			WHERE user_id = $1`,
			ui.UserID,
		)(&shop, &username, &password)

		if err == sql.ErrNoRows {
			return c, ErrNotFound
		} else if err != nil {
			return c, errors.Wrap(err, "error reading shop")
		}

		req, err = http.NewRequest(
			"POST",
			fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/custom_collections.json", shop, apiVersion),
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"custom_collection":{"title":"%s"}}`, ui.SetName))),
		)
		if err != nil {
			return c, errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return c, errors.Wrap(err, "error executing http call")
		}

		// TODO: handle errors

		var collection struct {
			CustomCollection Collection `json:"custom_collection"`
		}

		err = json.NewDecoder(resp.Body).Decode(&collection)

		_, err = exec(`INSERT INTO shopify_user_card_sets (
			card_set_id, user_id, shopify_collection_id, created_at, updated_at
		) VALUES ($1, $2, $3, now(), now())`, ui.CardSetID, ui.UserID, collection.CustomCollection.ID)

		return collection.CustomCollection, errors.Wrap(err, "error creating shopify user card set")
	}
}

func AddProductToCollection(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory, Collection, Product) error {
	return func(ui worker.UserInventory, c Collection, p Product) error {
		var (
			shop, username, password string

			req  *http.Request
			resp *http.Response

			err error
		)

		err = queryRow(`SELECT
			shop, api_key, password
			FROM user_shopifies
			WHERE user_id = $1`,
			ui.UserID,
		)(&shop, &username, &password)

		if err == sql.ErrNoRows {
			return ErrNotFound
		} else if err != nil {
			return errors.Wrap(err, "error reading shop")
		}

		req, err = http.NewRequest(
			"POST",
			fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/collects.json", shop, apiVersion),
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"collect":{"product_id":%d,"collection_id":%d}}`, p.ID, c.ID))),
		)
		if err != nil {
			return errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return errors.Wrap(err, "error executing http call")
		}

		if resp.StatusCode > http.StatusCreated {
			bodyBytes, _ := ioutil.ReadAll(resp.Body)
			return errors.New(string(bodyBytes))
		}

		return nil
	}
}

func RecordedClientDo(
	saveInteraction func(string, string, []byte, []byte) error,
) func(worker.UserInventory, *http.Request) (*http.Response, error) {
	return func(ui worker.UserInventory, req *http.Request) (*http.Response, error) {
		var (
			client *http.Client
			resp   *http.Response

			body, respBody []byte

			err error
		)

		client = http.DefaultClient

		if req.Body != nil {
			body, err = ioutil.ReadAll(req.Body)
			if err != nil {
				return nil, err
			}
			req.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		}

		resp, err = client.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		respBody, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		resp.Body = ioutil.NopCloser(bytes.NewBuffer(respBody))

		return resp, saveInteraction(ui.ID, fmt.Sprintf("%s %s", req.Method, req.URL.String()), body, respBody)
	}
}
