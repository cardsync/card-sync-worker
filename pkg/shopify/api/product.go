package api

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	worker "gitlab.com/cardsync/card-sync-worker"
)

type Product struct {
	ID       int              `json:"id"`
	Title    string           `json:"title"`
	Variants []ProductVariant `json:"variants"`
}

// FindProduct finds a shopify product object based on a synkt set id
func FindProduct(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory) (Product, error) {
	return func(ui worker.UserInventory) (Product, error) {
		var (
			shop, username, password string
			productID                int

			err error
		)

		err = queryRow(`SELECT
			shopify_product_id,
			shop, api_key, password
			FROM shopify_user_cards
			JOIN user_shopifies ON user_shopifies.user_id = shopify_user_cards.user_id
			WHERE shopify_user_cards.user_id = $1 AND card_id = $2`,
			ui.UserID, ui.CardID,
		)(&productID, &shop, &username, &password)

		if err == sql.ErrNoRows {
			return Product{}, ErrNotFound
		} else if err != nil {
			return Product{}, errors.Wrap(err, "error reading shopify product")
		}

		var (
			req  *http.Request
			resp *http.Response
		)

		req, err = http.NewRequest("GET", fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/products/%d.json", shop, apiVersion, productID), nil)
		if err != nil {
			return Product{}, errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return Product{}, errors.Wrap(err, "error executing http call")
		}

		if resp.StatusCode == http.StatusNotFound {
			return Product{}, ErrNotFound
		}

		var product struct {
			Product Product `json:"product"`
		}

		err = json.NewDecoder(resp.Body).Decode(&product)

		return product.Product, errors.Wrap(err, "error decoding json")
	}
}

// CreateProduct creates a shopify product object based on a synkt card id
func CreateProduct(
	queryRow func(query string, args ...interface{}) func(dest ...interface{}) error,
	exec func(string, ...interface{}) (sql.Result, error),
	apiVersion string,
	recordInteraction func(worker.UserInventory, *http.Request) (*http.Response, error),
) func(worker.UserInventory) (Product, error) {
	return func(ui worker.UserInventory) (Product, error) {
		var (
			shop               string
			username, password string
			p                  Product

			req  *http.Request
			resp *http.Response

			err error
		)

		err = queryRow(`SELECT
			shop, api_key, password
			FROM user_shopifies
			WHERE user_id = $1`,
			ui.UserID,
		)(&shop, &username, &password)

		if err == sql.ErrNoRows {
			return p, ErrNotFound
		} else if err != nil {
			return p, errors.Wrap(err, "error reading shop")
		}

		req, err = http.NewRequest(
			"POST",
			fmt.Sprintf("https://%s.myshopify.com/admin/api/%s/products.json", shop, apiVersion),
			bytes.NewBuffer([]byte(fmt.Sprintf(
				`{"product":{"title":"%s","variants":[{"option1":"%s","price":"%s","inventory_management": "shopify"}],"images":[{"src":"%s"}]}}`,
				ui.CardName, ui.Condition, ui.PriceString(), ui.PhotoURL(),
			))),
		)
		if err != nil {
			return p, errors.Wrap(err, "error creating http client")
		}

		req.SetBasicAuth(username, password)
		req.Header.Add("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		resp, err = recordInteraction(ui, req)
		if err != nil {
			return p, errors.Wrap(err, "error executing http call")
		}

		var product struct {
			Product Product `json:"product"`
		}

		err = json.NewDecoder(resp.Body).Decode(&product)

		_, err = exec(`INSERT INTO shopify_user_cards (
			card_id, user_id, shopify_product_id, created_at, updated_at
		) VALUES ($1, $2, $3, now(), now())`, ui.CardID, ui.UserID, product.Product.ID)
		if err != nil {
			return p, errors.Wrap(err, "error inserting shopify user cards")
		}

		_, err = exec(`INSERT INTO shopify_user_card_conditions (
			user_card_condition_id, shopify_product_variant_id, user_id, created_at, updated_at
		) VALUES ($1, $2, $3, now(), now())`, ui.ID, product.Product.Variants[0].ID, ui.UserID)

		return product.Product, errors.Wrap(err, "error creating shopify user card")
	}
}
