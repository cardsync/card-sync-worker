package api_test

import (
	"database/sql"

	"github.com/google/uuid"
	"github.com/pkg/errors"

	worker "gitlab.com/cardsync/card-sync-worker"
)

func createUserInventory(tx *sql.Tx) (worker.UserInventory, error) {
	var (
		ui worker.UserInventory

		userID, cardSetID, cardID            string
		cardConditionID, userCardConditionID string
		err                                  error
	)

	userID, cardSetID, cardID, cardConditionID, userCardConditionID = uuid.New().String(), uuid.New().String(), uuid.New().String(), uuid.New().String(), uuid.New().String()

	_, err = tx.Exec(`INSERT INTO users (id, email, created_at, updated_at, uid) VALUES ($1, 'foo@bar.com', now(), now(), 'uid')`, userID)
	if err != nil {
		return ui, errors.Wrap(err, "error creating user")
	}

	_, err = tx.Exec(`INSERT INTO card_sets (id, name, code, set_type, released_at, created_at, updated_at) VALUES ($1, 'set-name', 'set-code', 'set-type', '2001-01-01', now(), now())`, cardSetID)
	if err != nil {
		return ui, errors.Wrap(err, "error creating card set")
	}

	_, err = tx.Exec(`INSERT INTO cards (id, card_set_id, scryfall_id, name, type_line, rarity, created_at, updated_at) VALUES ($1, $2, 'scryfall-id', 'card-name', 'type_line', 'rarity', now(), now())`, cardID, cardSetID)
	if err != nil {
		return ui, errors.Wrap(err, "error creating card")
	}

	_, err = tx.Exec(`INSERT INTO card_conditions (id, card_id, condition, created_at, updated_at) VALUES ($1, $2, 'near-mint', now(), now())`, cardConditionID, cardID)
	if err != nil {
		return ui, errors.Wrap(err, "error creating card condition")
	}

	_, err = tx.Exec(`INSERT INTO user_card_conditions (id, user_id, card_condition_id, created_at, updated_at) VALUES ($1, $2, $3, now(), now())`, userCardConditionID, userID, cardConditionID)
	if err != nil {
		return ui, errors.Wrap(err, "error creating user card condition")
	}

	_, err = tx.Exec(`INSERT INTO user_shopifies (
		user_id, shop, api_key, password, created_at, updated_at
	) VALUES ($1, 'test', 'api-key', 'password', now(), now())`, userID)

	_, err = tx.Exec(`INSERT INTO shopify_user_card_sets(
		card_set_id, user_id, shopify_collection_id, created_at, updated_at
	) VALUES ($1, $2, 11, now(), now())`, cardSetID, userID)

	_, err = tx.Exec(`INSERT INTO shopify_user_cards(
		card_id, user_id, shopify_product_id, created_at, updated_at
	) VALUES ($1, $2, 11, now(), now())`, cardID, userID)

	return worker.UserInventory{
		ID:        userCardConditionID,
		UserID:    userID,
		CardID:    cardID,
		CardSetID: cardSetID,
	}, err
}
