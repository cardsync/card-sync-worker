package shopify

import (
	kitlog "github.com/go-kit/kit/log"

	worker "gitlab.com/cardsync/card-sync-worker"
	"gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"
)

type Service interface {
	Sync(string) error
}

func NewService(
	logger kitlog.Logger,
	hasShopify func(string) (bool, error),
	readUserInventory func(string, int) (worker.UserInventory, error),
	saveDelayedSync func(string) error,

	findCollection func(worker.UserInventory) (api.Collection, error), // find by set id
	createCollection func(worker.UserInventory) (api.Collection, error), // create by set id and name
	findProduct func(worker.UserInventory) (api.Product, error), // find by user id and card id
	createProduct func(worker.UserInventory) (api.Product, error), // create by user id and card id
	findProductVariant func(worker.UserInventory) (api.ProductVariant, error), // find by user card condition id
	createProductVariant func(string, int, worker.UserInventory) (api.ProductVariant, error), // create by user card condition id
	updateProductVariant func(worker.UserInventory, api.ProductVariant) error,
	updateProductVariantQuantity func(worker.UserInventory, api.ProductVariant) error,
	addProductToCollection func(worker.UserInventory, api.Collection, api.Product) error,
) *service {
	return &service{
		logger:                       logger,
		hasShopify:                   hasShopify,
		readUserInventory:            readUserInventory,
		saveDelayedSync:              saveDelayedSync,
		findCollection:               findCollection,
		createCollection:             createCollection,
		findProduct:                  findProduct,
		createProduct:                createProduct,
		findProductVariant:           findProductVariant,
		createProductVariant:         createProductVariant,
		updateProductVariant:         updateProductVariant,
		updateProductVariantQuantity: updateProductVariantQuantity,
		addProductToCollection:       addProductToCollection,
	}
}

type service struct {
	logger            kitlog.Logger
	hasShopify        func(string) (bool, error)
	readUserInventory func(string, int) (worker.UserInventory, error)
	saveDelayedSync   func(string) error

	findCollection               func(worker.UserInventory) (api.Collection, error)                  // find by set id
	createCollection             func(worker.UserInventory) (api.Collection, error)                  // create by set id and name
	findProduct                  func(worker.UserInventory) (api.Product, error)                     // find by user id and card id
	createProduct                func(worker.UserInventory) (api.Product, error)                     // create by user id and card id
	findProductVariant           func(worker.UserInventory) (api.ProductVariant, error)              // find by user card condition id
	createProductVariant         func(string, int, worker.UserInventory) (api.ProductVariant, error) // create by user card condition id
	updateProductVariant         func(worker.UserInventory, api.ProductVariant) error
	updateProductVariantQuantity func(worker.UserInventory, api.ProductVariant) error
	addProductToCollection       func(worker.UserInventory, api.Collection, api.Product) error
}

func (s *service) Sync(userInventoryID string) error {
	var (
		ui worker.UserInventory

		c api.Collection
		p api.Product
		v api.ProductVariant

		err error
	)

	ui, err = s.readUserInventory(userInventoryID, 1)
	if err != nil {
		s.saveDelayedSync(userInventoryID)
		return err
	}

	if ok, err := s.hasShopify(ui.UserID); !ok && err == nil {
		s.logger.Log("msg", "no shopify integration found, no action taken", "user_id", ui.UserID, "user_card_condition_id", userInventoryID)

		return nil
	} else if err != nil {
		return err
	}

	c, err = s.findCollection(ui)
	if err != nil && err != api.ErrNotFound {
		return err
	}

	if err == api.ErrNotFound {
		c, err = s.createCollection(ui)
		if err != nil {
			return err
		}
	}

	p, err = s.findProduct(ui)
	if err != nil && err != api.ErrNotFound {
		return err
	}

	if err == api.ErrNotFound {
		p, err = s.createProduct(ui)
		if err != nil {
			return err
		}

		err = s.addProductToCollection(ui, c, p)
		if err != nil {
			return err
		}
	}

	v, err = s.findProductVariant(ui)
	if err != nil && err != api.ErrNotFound {
		return err
	}

	if err == api.ErrNotFound {
		v, err = s.createProductVariant(ui.UserID, p.ID, ui)
		if err != nil {
			return err
		}
	} else {
		err = s.updateProductVariant(ui, v)
	}

	return s.updateProductVariantQuantity(ui, v)
}
