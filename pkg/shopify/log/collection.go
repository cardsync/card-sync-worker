package log

import (
	"time"

	"github.com/go-kit/kit/log"

	worker "gitlab.com/cardsync/card-sync-worker"
	"gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"
)

func FindCollection(
	logger log.Logger,
	next func(worker.UserInventory) (api.Collection, error),
) func(worker.UserInventory) (api.Collection, error) {
	return func(ui worker.UserInventory) (c api.Collection, err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "FindCollection",
				"ui", ui,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		c, err = next(ui)

		return
	}
}

func CreateCollection(
	logger log.Logger,
	next func(worker.UserInventory) (api.Collection, error),
) func(worker.UserInventory) (api.Collection, error) {
	return func(ui worker.UserInventory) (c api.Collection, err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "CreateCollection",
				"ui", ui,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		c, err = next(ui)

		return
	}
}

func AddProductToCollection(
	logger log.Logger,
	next func(worker.UserInventory, api.Collection, api.Product) error,
) func(worker.UserInventory, api.Collection, api.Product) error {
	return func(ui worker.UserInventory, c api.Collection, p api.Product) (err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "AddProductToCollection",
				"ui", ui,
				"c", c,
				"p", p,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		err = next(ui, c, p)

		return
	}
}
