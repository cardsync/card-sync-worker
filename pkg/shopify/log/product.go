package log

import (
	"time"

	"github.com/go-kit/kit/log"

	worker "gitlab.com/cardsync/card-sync-worker"
	"gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"
)

func FindProduct(
	logger log.Logger,
	next func(worker.UserInventory) (api.Product, error),
) func(worker.UserInventory) (api.Product, error) {
	return func(ui worker.UserInventory) (p api.Product, err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "FindProduct",
				"ui", ui,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		p, err = next(ui)

		return
	}
}

func CreateProduct(
	logger log.Logger,
	next func(worker.UserInventory) (api.Product, error),
) func(worker.UserInventory) (api.Product, error) {
	return func(ui worker.UserInventory) (p api.Product, err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "CreateProduct",
				"ui", ui,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		p, err = next(ui)

		return
	}
}
