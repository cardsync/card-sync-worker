package log

import (
	"time"

	"github.com/go-kit/kit/log"

	worker "gitlab.com/cardsync/card-sync-worker"
	"gitlab.com/cardsync/card-sync-worker/pkg/shopify/api"
)

func FindProductVariant(
	logger log.Logger,
	next func(worker.UserInventory) (api.ProductVariant, error),
) func(worker.UserInventory) (api.ProductVariant, error) {
	return func(ui worker.UserInventory) (pv api.ProductVariant, err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "FindProductVariant",
				"ui", ui,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		pv, err = next(ui)

		return
	}
}

func CreateProductVariant(
	logger log.Logger,
	next func(string, int, worker.UserInventory) (api.ProductVariant, error),
) func(string, int, worker.UserInventory) (api.ProductVariant, error) {
	return func(userID string, productID int, ui worker.UserInventory) (pv api.ProductVariant, err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "CreateProductVariant",
				"userID", userID,
				"productID", productID,
				"ui", ui,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		pv, err = next(userID, productID, ui)

		return
	}
}

func UpdateProductVariant(
	logger log.Logger,
	next func(worker.UserInventory, api.ProductVariant) error,
) func(worker.UserInventory, api.ProductVariant) error {
	return func(ui worker.UserInventory, pv api.ProductVariant) (err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "UpdateProductVariant",
				"ui", ui,
				"pv", pv,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		err = next(ui, pv)

		return
	}
}

func UpdateProductVariantQuantity(
	logger log.Logger,
	next func(worker.UserInventory, api.ProductVariant) error,
) func(worker.UserInventory, api.ProductVariant) error {
	return func(ui worker.UserInventory, pv api.ProductVariant) (err error) {
		defer func(begin time.Time) {
			_ = logger.Log(
				"service", "shopify",
				"method", "UpdateProductVariantQuantity",
				"ui", ui,
				"pv", pv,
				"err", err,
				"took", time.Since(begin),
			)
		}(time.Now())

		err = next(ui, pv)

		return
	}
}
