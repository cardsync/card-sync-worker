package db

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stripe/stripe-go"

	worker "gitlab.com/cardsync/card-sync-worker"
	"gitlab.com/cardsync/card-sync-worker/ebay"
)

type Service interface {
	ReadToken(string) (ebay.Token, error)
	SaveToken(ebay.Token) error
	ReadUserInventory(string, int) (worker.UserInventory, error)
	ReadOfferID(string) (worker.UserInventory, error)
	SaveOffer(worker.UserInventory, string, int) error
	SaveListingID(string, string, int) error
	SaveEbayInteraction(string, string, []byte, []byte) error
	SaveShopifyInteraction(string, string, []byte, []byte) error
	UserInPlanLimit(string, string, func(string, *stripe.CustomerParams) (*stripe.Customer, error)) (bool, error)
	HasEbay(string) (bool, error)
	HasShopify(string) (bool, error)
	SaveDelayedSync(string) error
	SetRefreshedAt(string) error
}

type service struct {
	db *sql.DB
}

func NewService(db *sql.DB) *service {
	return &service{db}
}

func (s *service) ReadToken(userID string) (ebay.Token, error) {
	var (
		t   ebay.Token
		err error
	)

	err = s.db.QueryRow(`SELECT
		user_id,
		auth_token,
		auth_token_expires_at,
		refresh_token,
		refresh_token_expires_at
		FROM user_ebays
		WHERE user_id = $1`,
		userID,
	).Scan(&t.UserID, &t.AccessToken, &t.ExpiresAt, &t.RefreshToken, &t.RefreshTokenExpiresAt)

	return t, errors.Wrap(err, "error reading ebay token")
}

func (s *service) SaveToken(t ebay.Token) error {
	var err error

	_, err = s.db.Exec(`UPDATE user_ebays SET
		auth_token = $2,
		auth_token_expires_at = $3,
		updated_at = $4
		WHERE user_id = $1`,
		t.UserID,
		t.AccessToken,
		t.ExpiresAt,
		time.Now(),
	)

	return errors.Wrap(err, "error saving ebay token")
}

func (s *service) ReadUserInventory(id string, qty int) (worker.UserInventory, error) {
	var (
		ui                  worker.UserInventory
		template            sql.NullString
		paymentPolicyID     sql.NullString
		returnPolicyID      sql.NullString
		fulfillmentPolicyID sql.NullString
		err                 error
	)

	err = s.db.QueryRow(
		`SELECT
			user_card_conditions.id,
			users.id,
			user_card_conditions.quantity,
			user_card_conditions.price,
			card_conditions.condition,
			cards.name,
			card_sets.name,
			COALESCE(ebay_offers.offer_id, ''),
			cards.id,
			card_sets.id,
			template,
			user_ebays.payment_policy_id,
			user_ebays.return_policy_id,
			user_ebays.fulfillment_policy_id,
			image
		FROM user_card_conditions
		JOIN users ON users.id = user_card_conditions.user_id
		JOIN card_conditions ON card_conditions.id = user_card_conditions.card_condition_id
		JOIN cards ON cards.id = card_conditions.card_id
		JOIN card_sets ON card_sets.id = cards.card_set_id
		LEFT JOIN ebay_offers ON ebay_offers.user_card_condition_id = user_card_conditions.id AND ebay_offers.quantity = $2
		JOIN user_ebays ON user_ebays.user_id = users.id
		WHERE user_card_conditions.id = $1`, id, qty,
	).Scan(
		&ui.ID,
		&ui.UserID,
		&ui.Qty,
		&ui.Price,
		&ui.Condition,
		&ui.CardName,
		&ui.SetName,
		&ui.OfferID,
		&ui.CardID,
		&ui.CardSetID,
		&template,
		&paymentPolicyID,
		&returnPolicyID,
		&fulfillmentPolicyID,
		&ui.Image,
	)

	ui.ListingQuantity = qty

	if template.Valid {
		ui.Template = &template.String
	}

	if paymentPolicyID.Valid {
		ui.PaymentPolicyID = paymentPolicyID.String
	}

	if returnPolicyID.Valid {
		ui.ReturnPolicyID = returnPolicyID.String
	}

	if fulfillmentPolicyID.Valid {
		ui.FulfillmentPolicyID = fulfillmentPolicyID.String
	}

	return ui, errors.Wrap(err, "error reading user inventory")
}

func (s *service) ReadOfferID(offerID string) (worker.UserInventory, error) {
	var (
		ui                  worker.UserInventory
		template            sql.NullString
		paymentPolicyID     sql.NullString
		returnPolicyID      sql.NullString
		fulfillmentPolicyID sql.NullString
		err                 error
	)

	err = s.db.QueryRow(
		`SELECT
			user_card_conditions.id,
			users.id,
			user_card_conditions.quantity,
			user_card_conditions.price,
			card_conditions.condition,
			cards.name,
			card_sets.name,
			COALESCE(ebay_offers.offer_id, ''),
			cards.id,
			card_sets.id,
			template,
			user_ebays.payment_policy_id,
			user_ebays.return_policy_id,
			user_ebays.fulfillment_policy_id,
			image,
			ebay_offers.quantity
		FROM user_card_conditions
		JOIN users ON users.id = user_card_conditions.user_id
		JOIN card_conditions ON card_conditions.id = user_card_conditions.card_condition_id
		JOIN cards ON cards.id = card_conditions.card_id
		JOIN card_sets ON card_sets.id = cards.card_set_id
		JOIN ebay_offers ON ebay_offers.user_card_condition_id = user_card_conditions.id
		JOIN user_ebays ON user_ebays.user_id = users.id
		WHERE ebay_offers.offer_id = $1`, offerID,
	).Scan(
		&ui.ID,
		&ui.UserID,
		&ui.Qty,
		&ui.Price,
		&ui.Condition,
		&ui.CardName,
		&ui.SetName,
		&ui.OfferID,
		&ui.CardID,
		&ui.CardSetID,
		&template,
		&paymentPolicyID,
		&returnPolicyID,
		&fulfillmentPolicyID,
		&ui.Image,
		&ui.ListingQuantity,
	)

	if template.Valid {
		ui.Template = &template.String
	}

	if paymentPolicyID.Valid {
		ui.PaymentPolicyID = paymentPolicyID.String
	}

	if returnPolicyID.Valid {
		ui.ReturnPolicyID = returnPolicyID.String
	}

	if fulfillmentPolicyID.Valid {
		ui.FulfillmentPolicyID = fulfillmentPolicyID.String
	}

	return ui, errors.Wrap(err, "error reading offer id")
}

func (s *service) SaveOffer(ui worker.UserInventory, offerID string, qty int) error {
	var err error

	_, err = s.db.Exec(`INSERT INTO ebay_offers (
			user_id,
			user_card_condition_id,
			offer_id,
			created_at,
			updated_at,
			quantity
		) VALUES (
			$1, $2, $3, now(), now(), $4
		)`,
		ui.UserID,
		ui.ID,
		offerID,
		qty,
	)

	return errors.Wrap(err, "error saving offer")
}

func (s *service) SaveListingID(offerID, listingID string, qty int) error {
	var err error

	_, err = s.db.Exec(
		`UPDATE ebay_offers SET listing_id = $2 WHERE offer_id = $1 AND quantity = $3`,
		offerID,
		listingID,
		qty,
	)

	return errors.Wrap(err, "error saving listing id")
}

func (s *service) SaveEbayInteraction(userInventoryID, interactionType string, reqBody, respBody []byte) error {
	var err error

	_, err = s.db.Exec(`INSERT INTO user_card_condition_ebay_api_histories (
			id,
			user_card_condition_id,
			interaction_type,
			api_request,
			api_response,
			created_at,
			updated_at
		) VALUES (
			$1, $2, $3, $4, $5, now(), now()
		)`,
		uuid.New().String(),
		userInventoryID,
		interactionType,
		reqBody,
		respBody,
	)

	return errors.Wrap(err, "error saving ebay api interaction")
}

func (s *service) SaveShopifyInteraction(userInventoryID, interactionType string, reqBody, respBody []byte) error {
	var err error

	_, err = s.db.Exec(`INSERT INTO user_card_condition_shopify_api_histories (
			id,
			user_card_condition_id,
			interaction_type,
			api_request,
			api_response,
			created_at,
			updated_at
		) VALUES (
			$1, $2, $3, $4, $5, now(), now()
		)`,
		uuid.New().String(),
		userInventoryID,
		interactionType,
		reqBody,
		respBody,
	)

	return errors.Wrap(err, "error saving shopify api interaction")
}

func (s *service) UserInPlanLimit(
	ucc, basicPlan string,
	getStripeCustomer func(string, *stripe.CustomerParams) (*stripe.Customer, error),
) (bool, error) {
	return true, nil
}

func (s *service) HasEbay(u string) (bool, error) {
	var (
		id  string
		err error
	)

	err = s.db.QueryRow(`SELECT id FROM user_ebays WHERE user_id = $1`, u).Scan(&id)

	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

func (s *service) HasShopify(u string) (bool, error) {
	var (
		id  string
		err error
	)

	err = s.db.QueryRow(`SELECT id FROM user_shopifies WHERE user_id = $1`, u).Scan(&id)

	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

func (s *service) SaveDelayedSync(u string) error {
	var err error

	_, err = s.db.Exec(
		`INSERT INTO user_card_conditions_delayed_sync (
      user_card_condition_id, sync_at, created_at, updated_at
    ) VALUES (
      $1, now() + interval '15 minutes', now(), now()
    )`, u,
	)

	return errors.Wrap(err, "error creating delayed sync")
}

func (s *service) SetRefreshedAt(offerID string) error {
	var err error

	_, err = s.db.Exec(`UPDATE ebay_offers SET refreshed_at = now() WHERE offer_id = $1`, offerID)

	return errors.Wrap(err, "error updating offer refreshed at")
}
