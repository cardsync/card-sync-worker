package ebay

type InventoryItem struct {
	Availability         InventoryItemAvailability `json:"availability"`
	Condition            string                    `json:"condition"`
	ConditionDescription string                    `json:"conditionDescription"`
	Product              InventoryItemProduct      `json:"product"`
}

type InventoryItemAvailability struct {
	ShipToLocationAvailability InventoryItemAvailabilityShipToLocationAvailability `json:"shipToLocationAvailability"`
}

type InventoryItemAvailabilityShipToLocationAvailability struct {
	Quantity int `json:"quantity"`
}

type InventoryItemProduct struct {
	Title       string   `json:"title"`
	Brand       string   `json:"brand"`
	MPN         string   `json:"mpn"`
	ImageURLS   []string `json:"imageUrls"`
	Description string   `json:"description"`
}
