package ebay

type Offer struct {
	Sku                 string               `json:"sku"`
	AvailableQuantity   int                  `json:"availableQuantity"`
	CategoryID          string               `json:"categoryId"`
	ListingDescription  string               `json:"listingDescription"`
	MerchantLocationKey string               `json:"merchantLocationKey"`
	PricingSummary      OfferPricingSummary  `json:"pricingSummary"`
	MarketplaceID       string               `json:"marketplaceId"`
	Format              string               `json:"format"`
	ListingPolicies     OfferListingPolicies `json:"listingPolicies"`
}

type OfferPricingSummary struct {
	Price OfferPricingSummaryPrice `json:"price"`
}

type OfferPricingSummaryPrice struct {
	Value    string `json:"value"`
	Currency string `json:"currency"`
}

type OfferListingPolicies struct {
	PaymentPolicyID     string `json:"paymentPolicyId"`
	ReturnPolicyID      string `json:"returnPolicyId"`
	FulfillmentPolicyID string `json:"fulfillmentPolicyId"`
}
