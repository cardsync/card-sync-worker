package ebay

import "time"

type Token struct {
	UserID                string
	AccessToken           string
	ExpiresAt             time.Time
	RefreshToken          string
	RefreshTokenExpiresAt time.Time
}
