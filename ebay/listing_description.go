package ebay

import (
	"bytes"
	"html/template"

	worker "gitlab.com/cardsync/card-sync-worker"
)

const tpl = `
	<table>
		<tr>
			<td width="33%">
				<img src="{{ .PhotoURL }}" width="350">
			</td>
			<td>
				<h2>{{ .ListingQuantity }}x {{ .CardName }} from {{ .SetName }}</h2>
				<p>Up for sale is {{ .ListingQuantityText }} of the above card in {{ .Condition }} condition.</p>
				<p>Buy with confidence</p>
				<p>Photo is a stock photo.</p>
			</td>
	</table>
`

func listingDescription(ui worker.UserInventory) string {
	if ui.Template != nil {
		return *ui.Template
	}

	var (
		t   *template.Template
		out *bytes.Buffer
		err error
	)

	out = new(bytes.Buffer)

	t, err = template.New("listing-description").Parse(tpl)
	if err != nil {
		panic(err)
	}

	err = t.Execute(out, ui)
	if err != nil {
		panic(err)
	}

	return out.String()
}
