package ebay

import (
	"encoding/json"
)

type Errors struct {
	Errors []Error
}

func (e Errors) Error() string {
	b, _ := json.Marshal(e)

	return string(b)
}

type Error struct {
	ErrorID   string `json:"errorId"`
	Domain    string `json:"domain"`
	Subdomain string `json:"subdomain"`
	Category  string `json:"category"`
	Message   string `json:"message"`
}

func (e Error) Retriable() bool {
	if e.ErrorID == "25001" {
		return true
	}

	if e.ErrorID == "25019" && e.Message == "Cannot revise listing. We're having trouble updating your listing right now. Please try again later." {
		return true
	}

	return false
}
