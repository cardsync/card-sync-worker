package ebay

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	worker "gitlab.com/cardsync/card-sync-worker"
)

type API interface {
	PutInventoryItem(worker.UserInventory) error
	OfferPublished(worker.UserInventory) (bool, error)
	CreateOffer(worker.UserInventory) error
	PublishOffer(worker.UserInventory) error
	UpdateOffer(worker.UserInventory) error
}

type api struct {
	url              string
	appID, appSecret string
	readToken        func(string) (Token, error)
	saveToken        func(Token) error
	saveOffer        func(worker.UserInventory, string, int) error
	saveListingID    func(string, string, int) error
	saveInteraction  func(string, string, []byte, []byte) error
}

func NewAPI(
	url string,
	appID, appSecret string,
	readToken func(string) (Token, error),
	saveToken func(Token) error,
	saveOffer func(worker.UserInventory, string, int) error,
	saveListingID func(string, string, int) error,
	saveInteraction func(string, string, []byte, []byte) error,
) *api {
	return &api{
		url:             url,
		appID:           appID,
		appSecret:       appSecret,
		readToken:       readToken,
		saveToken:       saveToken,
		saveOffer:       saveOffer,
		saveListingID:   saveListingID,
		saveInteraction: saveInteraction,
	}
}

func (a *api) PutInventoryItem(ui worker.UserInventory) error {
	var (
		reqBody []byte
		resp    *http.Response
		err     error
	)

	reqBody, err = json.Marshal(InventoryItem{
		Availability: InventoryItemAvailability{
			ShipToLocationAvailability: InventoryItemAvailabilityShipToLocationAvailability{
				Quantity: ui.Quantity(),
			},
		},
		Condition:            "USED_EXCELLENT",
		ConditionDescription: ui.Condition,
		Product: InventoryItemProduct{
			Title:       ui.EbayTitle(),
			Brand:       "Wizards of the Coast",
			MPN:         "Does not apply",
			ImageURLS:   []string{ui.PhotoURL()},
			Description: listingDescription(ui),
		},
	})
	if err != nil {
		return errors.Wrap(err, "error encoding inventory item request body")
	}

	resp, err = a.makeRequest(
		ui,
		"PUT",
		fmt.Sprintf("sell/inventory/v1/inventory_item/%s", ui.SKU()),
		reqBody,
	)

	if err != nil && resp == nil {
		return err
	} else if err != nil {
		body, _ := ioutil.ReadAll(resp.Body)

		return errors.Errorf("error updating inventory item (%s): %s", err, body)
	}

	if resp.StatusCode != http.StatusNoContent {
		body, _ := ioutil.ReadAll(resp.Body)

		var e Errors

		json.Unmarshal(body, &e)

		return errors.Wrapf(e, "error updating inventory item: %s", body)
	}

	return nil
}

func (a *api) CreateOffer(ui worker.UserInventory) error {
	var (
		reqBody []byte
		resp    *http.Response
		err     error
	)

	reqBody, err = json.Marshal(Offer{
		Sku:                 ui.SKU(),
		AvailableQuantity:   ui.Quantity(),
		CategoryID:          "38292",
		ListingDescription:  listingDescription(ui),
		MerchantLocationKey: ui.UserID,
		PricingSummary: OfferPricingSummary{
			Price: OfferPricingSummaryPrice{
				Value:    strconv.FormatFloat(ui.EbayPrice(), 'f', 2, 64),
				Currency: "USD", // TODO: don't hardcode this
			},
		},
		MarketplaceID: "EBAY_US", // TODO: don't hardcode this
		Format:        "FIXED_PRICE",
		ListingPolicies: OfferListingPolicies{
			PaymentPolicyID:     ui.PaymentPolicyID,
			ReturnPolicyID:      ui.ReturnPolicyID,
			FulfillmentPolicyID: ui.FulfillmentPolicyID,
		},
	})
	if err != nil {
		return errors.Wrap(err, "error encoding create offer request body")
	}

	resp, err = a.makeRequest(
		ui,
		"POST",
		"sell/inventory/v1/offer",
		reqBody,
	)
	if err != nil {
		return errors.Wrap(err, "error sending create offer request")
	}

	if resp.StatusCode != http.StatusCreated {
		body, _ := ioutil.ReadAll(resp.Body)

		var e Errors

		json.Unmarshal(body, &e)

		return errors.Wrapf(e, "error creating offer: %s", body)
	}

	var offerResp struct {
		ID string `json:"offerId"`
	}

	err = json.NewDecoder(resp.Body).Decode(&offerResp)
	if err != nil {
		return errors.Wrap(err, "error decoding create offer response")
	}

	return a.saveOffer(ui, offerResp.ID, ui.ListingQuantity)
}

func (a *api) UpdateOffer(ui worker.UserInventory) error {
	var (
		reqBody []byte
		resp    *http.Response
		err     error
	)

	reqBody, err = json.Marshal(Offer{
		Sku:                 ui.SKU(),
		AvailableQuantity:   ui.Quantity(),
		CategoryID:          "38292",
		ListingDescription:  listingDescription(ui),
		MerchantLocationKey: ui.UserID,
		PricingSummary: OfferPricingSummary{
			Price: OfferPricingSummaryPrice{
				Value:    strconv.FormatFloat(ui.EbayPrice(), 'f', 2, 64),
				Currency: "USD", // TODO: don't hardcode this
			},
		},
		MarketplaceID: "EBAY_US", // TODO: don't hardcode this
		Format:        "FIXED_PRICE",
		ListingPolicies: OfferListingPolicies{
			PaymentPolicyID:     ui.PaymentPolicyID,
			ReturnPolicyID:      ui.ReturnPolicyID,
			FulfillmentPolicyID: ui.FulfillmentPolicyID,
		},
	})
	if err != nil {
		return errors.Wrap(err, "error encoding update offer request body")
	}

	resp, err = a.makeRequest(
		ui,
		"PUT",
		fmt.Sprintf("sell/inventory/v1/offer/%s", ui.OfferID),
		reqBody,
	)
	if err != nil {
		return errors.Wrap(err, "error sending update offer request")
	}

	if resp.StatusCode != http.StatusNoContent {
		body, _ := ioutil.ReadAll(resp.Body)

		var e Errors

		json.Unmarshal(body, &e)

		return errors.Wrapf(e, "error updating offer: %s", body)
	}

	return nil
}

func (a *api) OfferPublished(ui worker.UserInventory) (bool, error) {
	var (
		resp *http.Response
		err  error
	)

	resp, err = a.makeRequest(
		ui,
		"GET",
		fmt.Sprintf("sell/inventory/v1/offer/%s", ui.OfferID),
		nil,
	)

	if err != nil {
		return false, errors.Wrap(err, "error sending get offer request")
	}

	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)

		return false, errors.Errorf("error getting offer: %s", body)
	}

	var offer struct {
		Status string `json:"status"`
	}

	err = json.NewDecoder(resp.Body).Decode(&offer)

	return offer.Status == "PUBLISHED", errors.Wrap(err, "error decoding get policy")
}

func (a *api) PublishOffer(ui worker.UserInventory) error {
	var (
		resp *http.Response
		err  error
	)

	resp, err = a.makeRequest(
		ui,
		"POST",
		fmt.Sprintf("sell/inventory/v1/offer/%s/publish/", ui.OfferID),
		nil,
	)

	if err != nil {
		return errors.Wrap(err, "error sending publish offer request")
	}

	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)

		var e Errors

		json.Unmarshal(body, &e)

		return errors.Wrapf(e, "error publishing offer: %s", body)
	}

	var listing struct {
		ListingID string `json:"listingId"`
	}

	err = json.NewDecoder(resp.Body).Decode(&listing)

	if err == nil {
		err = a.saveListingID(ui.OfferID, listing.ListingID, ui.ListingQuantity)
	}

	return errors.Wrap(err, "error publishing offer")
}

func (a *api) makeRequest(ui worker.UserInventory, method, path string, body []byte) (*http.Response, error) {
	var (
		req                     *http.Request
		resp                    *http.Response
		respBody                []byte
		t                       Token
		err, saveInteractionErr error
	)

	t, err = a.currentToken(ui.UserID)
	if err != nil {
		return nil, err
	}

	client := http.DefaultClient
	url := fmt.Sprintf("%s/%s", a.url, path)

	if body != nil {
		req, err = http.NewRequest(method, url, bytes.NewBuffer(body))
	} else {
		req, err = http.NewRequest(method, url, nil)
	}

	if err != nil {
		return nil, err
	}

	if method != "GET" && body != nil {
		req.Header.Add("Content-Type", "application/json")
	}
	req.Header.Add("Content-Language", "en-US") // TODO: generalize this??
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", t.AccessToken))

	resp, err = client.Do(req)

	if err == nil {
		respBody, _ = ioutil.ReadAll(resp.Body)
		resp.Body = ioutil.NopCloser(bytes.NewBuffer(respBody))
	}

	saveInteractionErr = a.saveInteraction(ui.ID, fmt.Sprintf("%s %s", method, url), body, respBody)
	if saveInteractionErr != nil {
		return resp, errors.Wrap(saveInteractionErr, "error saving ebay interaction")
	}

	return resp, err
}

func (a *api) currentToken(userID string) (Token, error) {
	token, err := a.readToken(userID)
	if err != nil {
		return token, err
	}

	if time.Now().Add(-time.Minute).Before(token.ExpiresAt) {
		return token, nil
	}

	form := url.Values{}
	form.Add("grant_type", "refresh_token")
	form.Add("refresh_token", token.RefreshToken)
	form.Add("scope", "https://api.ebay.com/oauth/api_scope https://api.ebay.com/oauth/api_scope/sell.inventory https://api.ebay.com/oauth/api_scope/sell.account")

	req, _ := http.NewRequest(
		"POST",
		fmt.Sprintf("%s/identity/v1/oauth2/token", a.url),
		strings.NewReader(form.Encode()),
	)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(a.appID, a.appSecret)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return token, errors.Wrap(err, "error refreshing token")
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)

		return token, errors.Errorf("error refreshing token: %s", body)
	}

	var result struct {
		AccessToken string `json:"access_token"`
		ExpiresIn   int    `json:"expires_in"`
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return token, errors.Wrap(err, "error refreshing token")
	}

	token.AccessToken = result.AccessToken
	token.ExpiresAt = time.Now().Add(time.Second * time.Duration(result.ExpiresIn))

	return token, a.saveToken(token)
}
