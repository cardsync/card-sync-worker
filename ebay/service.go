package ebay

import (
	"fmt"

	kitlog "github.com/go-kit/kit/log"
	"github.com/pkg/errors"

	worker "gitlab.com/cardsync/card-sync-worker"
)

type Service interface {
	Sync(string) error
	RefreshListing(string) error
}

func NewService(
	logger kitlog.Logger,
	hasEbay func(string) (bool, error),
	readUserInventory func(string, int) (worker.UserInventory, error),
	readOfferID func(string) (worker.UserInventory, error),
	putInventory func(worker.UserInventory) error,
	offerPublished func(worker.UserInventory) (bool, error),
	createOffer func(worker.UserInventory) error,
	publishOffer func(worker.UserInventory) error,
	updateOffer func(worker.UserInventory) error,
	saveInteraction func(string, string, []byte, []byte) error,
	saveDelayedSync func(string) error,
	setRefreshedAt func(string) error,
) *service {
	return &service{
		logger:            logger,
		hasEbay:           hasEbay,
		readUserInventory: readUserInventory,
		readOfferID:       readOfferID,
		putInventory:      putInventory,
		offerPublished:    offerPublished,
		createOffer:       createOffer,
		publishOffer:      publishOffer,
		updateOffer:       updateOffer,
		saveInteraction:   saveInteraction,
		saveDelayedSync:   saveDelayedSync,
		setRefreshedAt:    setRefreshedAt,
	}
}

type service struct {
	logger            kitlog.Logger
	hasEbay           func(string) (bool, error)
	readUserInventory func(string, int) (worker.UserInventory, error)
	readOfferID       func(string) (worker.UserInventory, error)
	putInventory      func(worker.UserInventory) error
	offerPublished    func(worker.UserInventory) (bool, error)
	createOffer       func(worker.UserInventory) error
	publishOffer      func(worker.UserInventory) error
	updateOffer       func(worker.UserInventory) error
	saveInteraction   func(string, string, []byte, []byte) error
	saveDelayedSync   func(string) error
	setRefreshedAt    func(string) error
}

func isRetriable(err error) bool {
	type retry interface {
		Retriable() bool
	}
	re, ok := err.(retry)
	return ok && re.Retriable()
}

func (s *service) RefreshListing(offerID string) error {
	var (
		ui  worker.UserInventory
		err error
	)

	ui, err = s.readOfferID(offerID)
	if err != nil {
		return err
	}

	if ui.Qty > 0 {
		return errors.New("found inventory had quantity greater than zero, refusing to refresh")
	}

	ui.Qty = 1
	err = s.sync(ui)
	if err != nil && isRetriable(errors.Cause(err)) {
		s.saveDelayedSync(ui.ID)
		return err
	} else if err != nil {
		return err
	}

	ui.Qty = 0
	err = s.sync(ui)
	if err != nil && isRetriable(errors.Cause(err)) {
		s.saveDelayedSync(ui.ID)
		return err
	} else if err != nil {
		return err
	}

	return s.setRefreshedAt(offerID)
}

func (s *service) Sync(userInventoryID string) error {
	var (
		ui  worker.UserInventory
		err error
	)

	ui, err = s.readUserInventory(userInventoryID, 1)
	if err != nil {
		s.saveDelayedSync(userInventoryID)
		return err
	}

	if ok, err := s.hasEbay(ui.UserID); !ok && err == nil {
		s.logger.Log("msg", "no ebay integration found, no action taken", "user_id", ui.UserID, "user_card_condition_id", userInventoryID)

		return nil
	} else if err != nil {
		return err
	}

	if oneErr := s.sync(ui); oneErr != nil {
		s.logger.Log("msg", "error syncing inventory", "user_id", ui.UserID, "user_card_condition_id", userInventoryID, "err", oneErr)

		if isRetriable(errors.Cause(oneErr)) {
			err = s.saveDelayedSync(userInventoryID)
			if err != nil {
				return err
			}
		}
	}

	ui, err = s.readUserInventory(userInventoryID, 4)
	if err != nil {
		s.saveDelayedSync(userInventoryID)
		return err
	}
	if fourErr := s.sync(ui); fourErr != nil {
		s.logger.Log("msg", "error syncing inventory", "user_id", ui.UserID, "user_card_condition_id", userInventoryID, "err", fourErr)

		if isRetriable(errors.Cause(fourErr)) {
			err = s.saveDelayedSync(userInventoryID)
			if err != nil {
				return err
			}
		}
	}

	return err
}

func (s *service) sync(ui worker.UserInventory) error {
	var (
		published bool
		err       error
	)

	s.logger.Log("level", "info", "msg", "syncing inventory", "user_id", ui.UserID, "user_card_condition_id", ui.ID, "user_inventory", fmt.Sprintf("%#v", ui))

	if !ui.ListableOnEbay() && ui.Quantity() > 0 {
		s.logger.Log("level", "info", "msg", "Price is less than 0.99. Not syncing inventory", "user_id", ui.UserID, "user_card_condition_id", ui.ID, "user_inventory", fmt.Sprintf("%#v", ui))

		return nil
	}

	err = s.putInventory(ui)
	if err != nil {
		return err
	}

	if ui.OfferID != "" {
		err = s.updateOffer(ui)
		if err != nil {
			return err
		}
	} else if ui.Quantity() > 0 {
		err = s.createOffer(ui)
		if err != nil {
			return err
		}
	}

	ui, err = s.readUserInventory(ui.ID, ui.ListingQuantity)
	if err != nil {
		return err
	}

	if ui.OfferID != "" {
		published, err = s.offerPublished(ui)
		if err != nil {
			return err
		}

		if !published && ui.Quantity() > 0 {
			return s.publishOffer(ui)
		}
	}

	return err
}
