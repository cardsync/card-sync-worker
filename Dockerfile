FROM golang:1.12 AS builder

ENV GO111MODULE=on

COPY . /code
WORKDIR /code

RUN CGO_ENABLED=0 go build -mod vendor -o /go/bin/worker ./cmd/worker
RUN CGO_ENABLED=0 go build -mod vendor -o /go/bin/delayed-sync ./cmd/delayed-sync
RUN CGO_ENABLED=0 go build -mod vendor -o /go/bin/import-tcg-products ./cmd/import-tcg-products

RUN groupadd -g 999 go && useradd -r -u 999 -g go go

FROM alpine:latest as certs
RUN apk --update add ca-certificates

FROM scratch

COPY --from=builder /go/bin/worker /go/bin/worker
COPY --from=builder /go/bin/delayed-sync /go/bin/delayed-sync
COPY --from=builder /go/bin/import-tcg-products /go/bin/import-tcg-products

COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=0 /etc/passwd /etc/passwd
USER go

ENTRYPOINT ["/go/bin/worker"]
