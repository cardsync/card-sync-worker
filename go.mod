module gitlab.com/cardsync/card-sync-worker

go 1.12

require (
	github.com/aws/aws-sdk-go v1.20.15
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.1.1
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.10.0
	github.com/pkg/errors v0.8.1
	github.com/stripe/stripe-go v61.12.0+incompatible
	github.com/twinj/uuid v1.0.0
)
