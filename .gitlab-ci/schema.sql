--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO synkt;

--
-- Name: card_condition_price_histories; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.card_condition_price_histories (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    card_condition_id uuid NOT NULL,
    date timestamp with time zone NOT NULL,
    price integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.card_condition_price_histories OWNER TO synkt;

--
-- Name: card_conditions; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.card_conditions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    card_id uuid NOT NULL,
    condition text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.card_conditions OWNER TO synkt;

--
-- Name: card_sets; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.card_sets (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name character varying NOT NULL,
    code character varying NOT NULL,
    set_type character varying NOT NULL,
    released_at date NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.card_sets OWNER TO synkt;

--
-- Name: cards; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.cards (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    card_set_id uuid,
    scryfall_id text NOT NULL,
    name text NOT NULL,
    type_line text NOT NULL,
    rarity text NOT NULL,
    multiverseid integer,
    number text,
    names text[],
    mana_cost text,
    cmc double precision,
    colors text[],
    color_identity text[],
    supertypes text[],
    types text[],
    subtypes text[],
    text text,
    flavor text,
    artist text,
    power text,
    toughness text,
    loyalty integer,
    variations text[],
    watermark text,
    border text,
    reserved boolean,
    layout text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    photo text,
    collector_number text,
    image text
);


ALTER TABLE public.cards OWNER TO synkt;

--
-- Name: ebay_offers; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.ebay_offers (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    user_card_condition_id uuid NOT NULL,
    offer_id text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    listing_id text,
    quantity integer DEFAULT 1 NOT NULL,
    refreshed_at timestamp with time zone
);


ALTER TABLE public.ebay_offers OWNER TO synkt;

--
-- Name: ebay_transaction_histories; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.ebay_transaction_histories (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_card_condition_id uuid NOT NULL,
    quantity integer NOT NULL,
    transaction_id text NOT NULL,
    xml text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.ebay_transaction_histories OWNER TO synkt;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO synkt;

--
-- Name: shopify_user_card_conditions; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.shopify_user_card_conditions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_card_condition_id uuid NOT NULL,
    user_id uuid NOT NULL,
    shopify_product_variant_id bigint NOT NULL,
    created_at timestamp(6) with time zone NOT NULL,
    updated_at timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.shopify_user_card_conditions OWNER TO synkt;

--
-- Name: shopify_user_card_sets; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.shopify_user_card_sets (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    card_set_id uuid NOT NULL,
    user_id uuid NOT NULL,
    shopify_collection_id bigint NOT NULL,
    created_at timestamp(6) with time zone NOT NULL,
    updated_at timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.shopify_user_card_sets OWNER TO synkt;

--
-- Name: shopify_user_cards; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.shopify_user_cards (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    card_id uuid NOT NULL,
    user_id uuid NOT NULL,
    shopify_product_id bigint NOT NULL,
    created_at timestamp(6) with time zone NOT NULL,
    updated_at timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.shopify_user_cards OWNER TO synkt;

--
-- Name: tcgplayer_card_conditions; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.tcgplayer_card_conditions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    card_condition_id uuid NOT NULL,
    sku_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.tcgplayer_card_conditions OWNER TO synkt;

--
-- Name: user_addresses; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_addresses (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    street text NOT NULL,
    building text,
    city text NOT NULL,
    subnational text NOT NULL,
    postal_code text,
    country text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_addresses OWNER TO synkt;

--
-- Name: user_card_condition_ebay_api_histories; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_card_condition_ebay_api_histories (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_card_condition_id uuid NOT NULL,
    interaction_type text NOT NULL,
    api_request text NOT NULL,
    api_response text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_card_condition_ebay_api_histories OWNER TO synkt;

--
-- Name: user_card_condition_shopify_api_histories; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_card_condition_shopify_api_histories (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_card_condition_id uuid NOT NULL,
    interaction_type text NOT NULL,
    api_request text NOT NULL,
    api_response text NOT NULL,
    created_at timestamp(6) with time zone NOT NULL,
    updated_at timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.user_card_condition_shopify_api_histories OWNER TO synkt;

--
-- Name: user_card_conditions; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_card_conditions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    card_condition_id uuid NOT NULL,
    quantity integer DEFAULT 0 NOT NULL,
    price integer DEFAULT 0 NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_card_conditions OWNER TO synkt;

--
-- Name: user_card_conditions_delayed_sync; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_card_conditions_delayed_sync (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_card_condition_id uuid NOT NULL,
    sync_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_card_conditions_delayed_sync OWNER TO synkt;

--
-- Name: user_ebays; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_ebays (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    auth_token text NOT NULL,
    auth_token_expires_at timestamp with time zone NOT NULL,
    refresh_token text NOT NULL,
    refresh_token_expires_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    legacy_auth_token text,
    legacy_auth_token_expires_at timestamp with time zone,
    template text,
    payment_policy_id text,
    fulfillment_policy_id text,
    return_policy_id text
);


ALTER TABLE public.user_ebays OWNER TO synkt;

--
-- Name: user_shopifies; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_shopifies (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    shop character varying,
    api_key character varying,
    password character varying,
    shared_secret character varying,
    created_at timestamp(6) with time zone NOT NULL,
    updated_at timestamp(6) with time zone NOT NULL
);


ALTER TABLE public.user_shopifies OWNER TO synkt;

--
-- Name: user_tcgplayers; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.user_tcgplayers (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    access_token character varying NOT NULL,
    bearer_token character varying,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.user_tcgplayers OWNER TO synkt;

--
-- Name: users; Type: TABLE; Schema: public; Owner: synkt
--

CREATE TABLE public.users (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    stripe_customer_id text,
    stripe_subscription_id text,
    uid character varying NOT NULL,
    first_name text,
    last_name text
);


ALTER TABLE public.users OWNER TO synkt;

--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: card_condition_price_histories card_condition_price_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.card_condition_price_histories
    ADD CONSTRAINT card_condition_price_histories_pkey PRIMARY KEY (id);


--
-- Name: card_conditions card_conditions_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.card_conditions
    ADD CONSTRAINT card_conditions_pkey PRIMARY KEY (id);


--
-- Name: card_sets card_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.card_sets
    ADD CONSTRAINT card_sets_pkey PRIMARY KEY (id);


--
-- Name: cards cards_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- Name: ebay_offers ebay_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.ebay_offers
    ADD CONSTRAINT ebay_offers_pkey PRIMARY KEY (id);


--
-- Name: ebay_transaction_histories ebay_transaction_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.ebay_transaction_histories
    ADD CONSTRAINT ebay_transaction_histories_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: shopify_user_card_conditions shopify_user_card_conditions_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_card_conditions
    ADD CONSTRAINT shopify_user_card_conditions_pkey PRIMARY KEY (id);


--
-- Name: shopify_user_card_sets shopify_user_card_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_card_sets
    ADD CONSTRAINT shopify_user_card_sets_pkey PRIMARY KEY (id);


--
-- Name: shopify_user_cards shopify_user_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_cards
    ADD CONSTRAINT shopify_user_cards_pkey PRIMARY KEY (id);


--
-- Name: tcgplayer_card_conditions tcgplayer_card_conditions_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.tcgplayer_card_conditions
    ADD CONSTRAINT tcgplayer_card_conditions_pkey PRIMARY KEY (id);


--
-- Name: user_addresses user_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_addresses
    ADD CONSTRAINT user_addresses_pkey PRIMARY KEY (id);


--
-- Name: user_card_condition_ebay_api_histories user_card_condition_ebay_api_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_condition_ebay_api_histories
    ADD CONSTRAINT user_card_condition_ebay_api_histories_pkey PRIMARY KEY (id);


--
-- Name: user_card_condition_shopify_api_histories user_card_condition_shopify_api_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_condition_shopify_api_histories
    ADD CONSTRAINT user_card_condition_shopify_api_histories_pkey PRIMARY KEY (id);


--
-- Name: user_card_conditions_delayed_sync user_card_conditions_delayed_sync_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_conditions_delayed_sync
    ADD CONSTRAINT user_card_conditions_delayed_sync_pkey PRIMARY KEY (id);


--
-- Name: user_card_conditions user_card_conditions_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_conditions
    ADD CONSTRAINT user_card_conditions_pkey PRIMARY KEY (id);


--
-- Name: user_ebays user_ebays_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_ebays
    ADD CONSTRAINT user_ebays_pkey PRIMARY KEY (id);


--
-- Name: user_shopifies user_shopifies_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_shopifies
    ADD CONSTRAINT user_shopifies_pkey PRIMARY KEY (id);


--
-- Name: user_tcgplayers user_tcgplayers_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_tcgplayers
    ADD CONSTRAINT user_tcgplayers_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: card_condition_price_histories_idx; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX card_condition_price_histories_idx ON public.card_condition_price_histories USING btree (card_condition_id, date);


--
-- Name: ebay_offers_uniq_idx; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX ebay_offers_uniq_idx ON public.ebay_offers USING btree (user_id, user_card_condition_id, quantity);


--
-- Name: index_card_condition_price_histories_on_card_condition_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_card_condition_price_histories_on_card_condition_id ON public.card_condition_price_histories USING btree (card_condition_id);


--
-- Name: index_card_conditions_on_card_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_card_conditions_on_card_id ON public.card_conditions USING btree (card_id);


--
-- Name: index_cards_on_card_set_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_cards_on_card_set_id ON public.cards USING btree (card_set_id);


--
-- Name: index_cards_on_scryfall_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_cards_on_scryfall_id ON public.cards USING btree (scryfall_id);


--
-- Name: index_ebay_api_histories_user_card_condition; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_ebay_api_histories_user_card_condition ON public.user_card_condition_ebay_api_histories USING btree (user_card_condition_id);


--
-- Name: index_ebay_offers_on_offer_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_ebay_offers_on_offer_id ON public.ebay_offers USING btree (offer_id);


--
-- Name: index_ebay_offers_on_user_card_condition_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_ebay_offers_on_user_card_condition_id ON public.ebay_offers USING btree (user_card_condition_id);


--
-- Name: index_ebay_offers_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_ebay_offers_on_user_id ON public.ebay_offers USING btree (user_id);


--
-- Name: index_ebay_transaction_histories_on_user_card_condition_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_ebay_transaction_histories_on_user_card_condition_id ON public.ebay_transaction_histories USING btree (user_card_condition_id);


--
-- Name: index_shopify_api_histories_user_card_condition; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_api_histories_user_card_condition ON public.user_card_condition_shopify_api_histories USING btree (user_card_condition_id);


--
-- Name: index_shopify_user_card_conditions_on_user_card_condition_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_user_card_conditions_on_user_card_condition_id ON public.shopify_user_card_conditions USING btree (user_card_condition_id);


--
-- Name: index_shopify_user_card_conditions_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_user_card_conditions_on_user_id ON public.shopify_user_card_conditions USING btree (user_id);


--
-- Name: index_shopify_user_card_sets_on_card_set_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_user_card_sets_on_card_set_id ON public.shopify_user_card_sets USING btree (card_set_id);


--
-- Name: index_shopify_user_card_sets_on_card_set_id_and_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_shopify_user_card_sets_on_card_set_id_and_user_id ON public.shopify_user_card_sets USING btree (card_set_id, user_id);


--
-- Name: index_shopify_user_card_sets_on_shopify_collection_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_shopify_user_card_sets_on_shopify_collection_id ON public.shopify_user_card_sets USING btree (shopify_collection_id);


--
-- Name: index_shopify_user_card_sets_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_user_card_sets_on_user_id ON public.shopify_user_card_sets USING btree (user_id);


--
-- Name: index_shopify_user_cards_on_card_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_user_cards_on_card_id ON public.shopify_user_cards USING btree (card_id);


--
-- Name: index_shopify_user_cards_on_shopify_product_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_shopify_user_cards_on_shopify_product_id ON public.shopify_user_cards USING btree (shopify_product_id);


--
-- Name: index_shopify_user_cards_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_shopify_user_cards_on_user_id ON public.shopify_user_cards USING btree (user_id);


--
-- Name: index_shopify_user_cards_on_user_id_and_card_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_shopify_user_cards_on_user_id_and_card_id ON public.shopify_user_cards USING btree (user_id, card_id);


--
-- Name: index_tcgplayer_card_conditions_on_card_condition_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_tcgplayer_card_conditions_on_card_condition_id ON public.tcgplayer_card_conditions USING btree (card_condition_id);


--
-- Name: index_user_addresses_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_user_addresses_on_user_id ON public.user_addresses USING btree (user_id);


--
-- Name: index_user_card_condition_delayed_user_crd_idx; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_user_card_condition_delayed_user_crd_idx ON public.user_card_conditions_delayed_sync USING btree (user_card_condition_id);


--
-- Name: index_user_card_conditions_delayed_sync_on_sync_at; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_user_card_conditions_delayed_sync_on_sync_at ON public.user_card_conditions_delayed_sync USING btree (sync_at);


--
-- Name: index_user_card_conditions_on_card_condition_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_user_card_conditions_on_card_condition_id ON public.user_card_conditions USING btree (card_condition_id);


--
-- Name: index_user_card_conditions_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_user_card_conditions_on_user_id ON public.user_card_conditions USING btree (user_id);


--
-- Name: index_user_ebays_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_user_ebays_on_user_id ON public.user_ebays USING btree (user_id);


--
-- Name: index_user_shopifies_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_user_shopifies_on_user_id ON public.user_shopifies USING btree (user_id);


--
-- Name: index_user_tcgplayers_on_user_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_user_tcgplayers_on_user_id ON public.user_tcgplayers USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_stripe_customer_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_users_on_stripe_customer_id ON public.users USING btree (stripe_customer_id);


--
-- Name: index_users_on_stripe_subscription_id; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_users_on_stripe_subscription_id ON public.users USING btree (stripe_subscription_id);


--
-- Name: index_users_on_uid; Type: INDEX; Schema: public; Owner: synkt
--

CREATE INDEX index_users_on_uid ON public.users USING btree (uid);


--
-- Name: shopify_user_card_condition_shopify_variant_idx; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX shopify_user_card_condition_shopify_variant_idx ON public.shopify_user_card_conditions USING btree (shopify_product_variant_id);


--
-- Name: shopify_user_card_conditions_user_ucc_idx; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX shopify_user_card_conditions_user_ucc_idx ON public.shopify_user_card_conditions USING btree (user_id, user_card_condition_id);


--
-- Name: sku_id_card_condition_id_idx; Type: INDEX; Schema: public; Owner: synkt
--

CREATE UNIQUE INDEX sku_id_card_condition_id_idx ON public.tcgplayer_card_conditions USING btree (card_condition_id, sku_id);


--
-- Name: shopify_user_cards fk_rails_08816f0440; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_cards
    ADD CONSTRAINT fk_rails_08816f0440 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: ebay_offers fk_rails_1863c36368; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.ebay_offers
    ADD CONSTRAINT fk_rails_1863c36368 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: user_card_condition_shopify_api_histories fk_rails_1c1cf91cbb; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_condition_shopify_api_histories
    ADD CONSTRAINT fk_rails_1c1cf91cbb FOREIGN KEY (user_card_condition_id) REFERENCES public.user_card_conditions(id) ON DELETE CASCADE;


--
-- Name: user_card_conditions fk_rails_1c477dc301; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_conditions
    ADD CONSTRAINT fk_rails_1c477dc301 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: user_tcgplayers fk_rails_1f094b4536; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_tcgplayers
    ADD CONSTRAINT fk_rails_1f094b4536 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: user_addresses fk_rails_2718c5b54a; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_addresses
    ADD CONSTRAINT fk_rails_2718c5b54a FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: cards fk_rails_2b6dc2726c; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.cards
    ADD CONSTRAINT fk_rails_2b6dc2726c FOREIGN KEY (card_set_id) REFERENCES public.card_sets(id) ON DELETE CASCADE;


--
-- Name: shopify_user_card_sets fk_rails_3265160b6c; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_card_sets
    ADD CONSTRAINT fk_rails_3265160b6c FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: user_card_conditions_delayed_sync fk_rails_39b221c71e; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_conditions_delayed_sync
    ADD CONSTRAINT fk_rails_39b221c71e FOREIGN KEY (user_card_condition_id) REFERENCES public.user_card_conditions(id) ON DELETE CASCADE;


--
-- Name: user_ebays fk_rails_44ef79a359; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_ebays
    ADD CONSTRAINT fk_rails_44ef79a359 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: card_conditions fk_rails_5fee70a3a8; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.card_conditions
    ADD CONSTRAINT fk_rails_5fee70a3a8 FOREIGN KEY (card_id) REFERENCES public.cards(id) ON DELETE CASCADE;


--
-- Name: ebay_offers fk_rails_7b8cd20c8e; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.ebay_offers
    ADD CONSTRAINT fk_rails_7b8cd20c8e FOREIGN KEY (user_card_condition_id) REFERENCES public.user_card_conditions(id) ON DELETE CASCADE;


--
-- Name: card_condition_price_histories fk_rails_7d13adf432; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.card_condition_price_histories
    ADD CONSTRAINT fk_rails_7d13adf432 FOREIGN KEY (card_condition_id) REFERENCES public.card_conditions(id);


--
-- Name: user_card_conditions fk_rails_814558ae8b; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_conditions
    ADD CONSTRAINT fk_rails_814558ae8b FOREIGN KEY (card_condition_id) REFERENCES public.card_conditions(id) ON DELETE CASCADE;


--
-- Name: ebay_transaction_histories fk_rails_82f3002904; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.ebay_transaction_histories
    ADD CONSTRAINT fk_rails_82f3002904 FOREIGN KEY (user_card_condition_id) REFERENCES public.user_card_conditions(id) ON DELETE CASCADE;


--
-- Name: shopify_user_card_conditions fk_rails_9007a61ee6; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_card_conditions
    ADD CONSTRAINT fk_rails_9007a61ee6 FOREIGN KEY (user_card_condition_id) REFERENCES public.user_card_conditions(id) ON DELETE CASCADE;


--
-- Name: shopify_user_card_sets fk_rails_97ffa1dfc0; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_card_sets
    ADD CONSTRAINT fk_rails_97ffa1dfc0 FOREIGN KEY (card_set_id) REFERENCES public.card_sets(id) ON DELETE CASCADE;


--
-- Name: tcgplayer_card_conditions fk_rails_a5e6f70a5e; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.tcgplayer_card_conditions
    ADD CONSTRAINT fk_rails_a5e6f70a5e FOREIGN KEY (card_condition_id) REFERENCES public.card_conditions(id);


--
-- Name: user_shopifies fk_rails_b73a11ebbc; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_shopifies
    ADD CONSTRAINT fk_rails_b73a11ebbc FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: shopify_user_cards fk_rails_e9715409f3; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_cards
    ADD CONSTRAINT fk_rails_e9715409f3 FOREIGN KEY (card_id) REFERENCES public.cards(id) ON DELETE CASCADE;


--
-- Name: shopify_user_card_conditions fk_rails_ec93fcbb38; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.shopify_user_card_conditions
    ADD CONSTRAINT fk_rails_ec93fcbb38 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: user_card_condition_ebay_api_histories fk_rails_feb4020efa; Type: FK CONSTRAINT; Schema: public; Owner: synkt
--

ALTER TABLE ONLY public.user_card_condition_ebay_api_histories
    ADD CONSTRAINT fk_rails_feb4020efa FOREIGN KEY (user_card_condition_id) REFERENCES public.user_card_conditions(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

