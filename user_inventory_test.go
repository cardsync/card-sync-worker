package worker_test

import (
	. "gitlab.com/cardsync/card-sync-worker"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("UserInventory", func() {
	var subject UserInventory

	Describe("EbayPrice", func() {
		JustBeforeEach(func() {
			subject = UserInventory{ListingQuantity: 4, Price: 212}
		})

		It("Multiplies the quantity by the price", func() {
			Expect(subject.EbayPrice()).To(Equal(8.48))
		})
	})

	Describe("ListableOnEbay", func() {
		Describe("When the listing quantity is 0", func() {
			JustBeforeEach(func() {
				subject = UserInventory{ListingQuantity: 0, Price: 212}
			})

			It("Returns false", func() {
				Expect(subject.ListableOnEbay()).To(BeFalse())
			})
		})

		Describe("When the price is less than 99", func() {
			JustBeforeEach(func() {
				subject = UserInventory{ListingQuantity: 1, Price: 98}
			})

			It("Returns false", func() {
				Expect(subject.ListableOnEbay()).To(BeFalse())
			})
		})

		Describe("When the price is more than 98 and the quantity is more than 0", func() {
			JustBeforeEach(func() {
				subject = UserInventory{ListingQuantity: 1, Price: 99}
			})

			It("Returns true", func() {
				Expect(subject.ListableOnEbay()).To(BeTrue())
			})
		})
	})

	Describe("EbayTitle", func() {
		Describe("When the title is less than 80 characters", func() {
			JustBeforeEach(func() {
				subject = UserInventory{
					ListingQuantity: 4,
					CardName:        "card",
					SetName:         "set",
					Condition:       "condition",
				}
			})

			It("Formats the ebay title", func() {
				Expect(subject.EbayTitle()).To(Equal("4x MTG card - set (condition)"))
			})
		})

		Describe("When the title is more than 80 characters", func() {
			JustBeforeEach(func() {
				subject = UserInventory{
					ListingQuantity: 4,
					CardName:        "cardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcard",
					SetName:         "set",
					Condition:       "condition",
				}
			})

			It("Truncates to 80 characters", func() {
				Expect(subject.EbayTitle()).To(Equal("4x MTG cardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardcardca..."))
			})
		})
	})
})
